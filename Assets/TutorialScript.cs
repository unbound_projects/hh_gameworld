﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialScript : MonoBehaviour {

    public int index;

    public List<GameObject> buttonList = new List<GameObject>();
    public List<string> textList = new List<string>();

    public List<GameObject> popupList = new List<GameObject>();

    public GameObject focusObject;
    public GameObject focusButton;
    public GameObject arrow;
    public bool isTut = true;
    GameObject tut;

    TutorialManager tutManager;
    GameSystem gameSys;

    // Use this for initialization
    public void SetStart ()
    {
        tutManager = GameObject.FindObjectOfType<TutorialManager>();
        gameSys = GameObject.FindObjectOfType<GameSystem>();
        index = GameObject.FindObjectOfType<TutorialManager>().index;

        if (gameSys.ulCrops.Count == 0)
        {
            gameSys.ulCrops.Add(gameSys.lCrops[0]);
            gameSys.lCrops.Remove(gameSys.lCrops[0]);
        }

        // These are all text/strings for the Tutorial panels. 
        textList.Add("Welcome to Hectic Harvest! Let's begin the tutorial with the basics, shall we? Start by clicking on the Summer Icon.");
        textList.Add("Look at all the different seasons! After selecting our season, we can select the level we wish to play. For now, lets play Level 1.");
        textList.Add("As you can see, each level has different 'Objectives'. Lets see if we can get the objective for the Beetroot for this level!");

        textList.Add("To plant or harvest a crop, you just need to click on the plot. Be sure to wait for your crop to grow before harvesting!");
        textList.Add("Harvesting vegetables at the right time is crucial to obtain a good score! Crops provide the most score at their 3rd stage of evolution.");
        textList.Add("Oh, don't forget to get rid of the nasty bugs when they pop up, or they'll eat all your crops! Make sure to stop them as soon as you can!");
        
        textList.Add("Now that you've completed your first level, it's time to get your some new crops! Lets buy a new seed!");
        textList.Add("Lets go and buy some corn seeds, since its the cheapest option!");
        textList.Add("We can now use corn seeds in our levels! Lets try out another level.");
        textList.Add("Let's keep going with the Summer season! Don't forget, you can buy other seasons from the shop, once you have enough gold!");
        textList.Add("Now that you've finished the first level, let's continue on with the second.");
        textList.Add("Both the Turnip and the Corn are objectives of this level, so our 'pouch' has adjusted to this. Lets begin!");

        textList.Add("Before progressing any further, lets cover the 'pouch' mechanic.");
        textList.Add("After purchasing a seed you can adjust your pouch to fits your needs. Having multiple of one crops helps complete single type objectives"); // THIS IS SELF BUTTON
        textList.Add("However if you have a pouch with all different types of seeds, you will get a high multipler helping you complete the score objective"); // This is a self button
        textList.Add("Now that that has been explained you have now completed the tutorial for Hectic Harvest. Have fun harvesting!!!");
    }
	
	// Update is called once per frame
	public void SetUpdate ()
    {
        // If we're working in the game-level scene
        if (index == 3 && focusObject == null && SceneManager.GetActiveScene().buildIndex == 1)
        {
            tut = this.gameObject;
            tutManager.gameObject.GetComponent<Canvas>().worldCamera = GameObject.Find("Camera").GetComponent<Camera>();

            for (int i = 0; i < tut.transform.childCount; ++i)
            {
                popupList[index + i] = (tut.transform.GetChild(i).gameObject);
                buttonList[index + i] = (tut.transform.GetChild(i).GetChild(2).gameObject);
            }

        }

        // If we're on the main menu
        else if (index == 6 && focusObject == null && SceneManager.GetActiveScene().buildIndex == 0)
        {
            tut = gameObject;
            tutManager.gameObject.GetComponent<Canvas>().worldCamera = Camera.main.gameObject.transform.GetChild(0).GetComponent<Camera>();
        }
            if (focusButton != null)
            {
                ColorBlock c = focusButton.GetComponent<Button>().colors;
                Color colChange = new Color(c.normalColor.r, c.normalColor.g, c.normalColor.b, .7f + Mathf.PingPong(Time.time * 1f, .3f));
                c.normalColor = colChange;
                focusButton.GetComponent<Button>().colors = c;
            }
            if (index < buttonList.Count && focusButton == null)
            {
                if (tut != null && index >= 3 || index < 3)
                {
                    FocusButton();
                }
            }
            else if (index >= buttonList.Count)
            {
                gameObject.SetActive(false);
            }
	}

    public void FocusButton()
    {
        GameObject listIndex = popupList[index];

        if (listIndex != null)
        {
            // Set this button active
            listIndex.SetActive(true);

            // Set the icon to full opacity
            Color c = popupList[index].GetComponent<Image>().color;
            c.a = 255;
            listIndex.GetComponent<Image>().color = c;

            listIndex.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Tutorial " + (index + 1).ToString();
            listIndex.transform.GetChild(1).gameObject.GetComponent<Text>().text = textList[index];

            if (index == 3 || index == 4 || index == 5)
            {
                focusObject = Instantiate(popupList[index], tutManager.gameObject.transform) as GameObject;
                focusButton = focusObject.transform.GetChild(2).gameObject;

                focusButton.GetComponent<Button>().onClick.AddListener(() => DestroyButton());
                focusObject.transform.SetSiblingIndex(1);
                
                Time.timeScale = 0;
                tutManager.transform.GetChild(0).gameObject.SetActive(true);
            }
            else if (SceneManager.GetActiveScene().buildIndex == 0)
            {
                focusButton = Instantiate(buttonList[index], tutManager.gameObject.transform) as GameObject;
                focusButton.transform.SetSiblingIndex(1);

                focusButton.GetComponent<Button>().onClick.AddListener(() => DestroyButton());
                focusButton.GetComponent<Image>().color = new Color(255, 255, 255, 255);
            }
        }
    }

    public void DestroyButton()
    {
        TutorialManager tutManager = GameObject.FindObjectOfType<TutorialManager>();

        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            focusButton = null;
            Destroy(focusObject);
            Time.timeScale = 1;
            tutManager.transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            Destroy(focusButton);
        }
        popupList[index].SetActive(false);
        tutManager.index++;
        index = tutManager.index;
    }
}
