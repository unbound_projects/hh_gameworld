﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TutorialManager : MonoBehaviour {

    TutorialScript tutorial;
    public bool isTut = false;
    public int index;
	// Use this for initialization
	void Start ()
    {
        if (GameObject.FindObjectOfType<GameSystem>().ulCrops.Count > 0)
        {
            //GameObject.FindObjectOfType<GameSystem>()
            isTut = false;
            gameObject.SetActive(false);
        }
        else
        {
            isTut = true;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isTut)
        {
            if (tutorial != null)
            {
                if (!tutorial.gameObject.activeSelf)
                {
                    Destroy(this.gameObject);
                }
                tutorial.SetUpdate();
            }
            else
            {
                tutSet();
                //tutorial.FocusButton();
            }

        }
	}

    public void tutSet()
    {
        tutorial = GameObject.FindObjectOfType<TutorialScript>();
        tutorial.SetStart();
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            Time.timeScale = 1;
        }
        else
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
