﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PlantIdentity : MonoBehaviour {

    public ObjectiveCrops.PlantDeclare plantType;

    public bool purchased = false;

    public bool isPlant = true;

    public LevelData.Seasons seasonType;

    public void ActivateTexture()
    {
        GetComponent<Image>().color = new Vector4(203,203,203,255);
        purchased = true;
    }

    public void ChangedType()
    {
        if (isPlant)
        {
            if (plantType == ObjectiveCrops.PlantDeclare.NULL)
            {
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(true);
            }
        }
        else
        {
            if (seasonType == LevelData.Seasons.NULL )
            {
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(true);
            }
        }
    }
   
}
