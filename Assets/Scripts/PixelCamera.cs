﻿using UnityEngine;
 using System.Collections;
 
 public class PixelCamera: MonoBehaviour {
 
     public RenderTexture renderTexture;
 
     void Start() {
        GetComponent<Camera>().targetTexture = renderTexture;
        renderTexture.width = Screen.width;
        renderTexture.height = Screen.height;
        Debug.Log("(Pixelation)(Start)renderTexture.width: " + renderTexture.width);
     }
 }
 