﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlotTypes : MonoBehaviour {

    public List<GameObject> AllowedPlotObjects = new List<GameObject>();
    public List<ObjectiveCrops.PlantDeclare> presentCrops = new List<ObjectiveCrops.PlantDeclare>();
}
