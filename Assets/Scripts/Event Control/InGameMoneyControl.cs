﻿using UnityEngine;
using System.Collections;

public class InGameMoneyControl : MonoBehaviour
{
    ObserverObject observer;

    private int playersCurrentMoney;

    void Start ()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        observer.AddSubscriber("UpdateMoney", gameObject, "UpdateCurrentMoney");
	}

    void UpdateCurrentMoney(int moneyPassed)
    {
        playersCurrentMoney += moneyPassed;
        observer.DoEvent("BroadcastPlayerMoney", playersCurrentMoney);
    }
}