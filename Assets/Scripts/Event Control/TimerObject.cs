﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TimerObject : MonoBehaviour
{
    ObserverObject observer;

    public class TimerData
    {
        public string name = "unset";
        public float timeUntilFired = 0.0f;
        public float fullTimerTime = 0.0f;
        public GameObject gameObject = null;
        public string functionToCall = "invalid";
        public bool shouldRepeat = false;
    }

    Dictionary<GameObject, List<TimerData>> timers = new Dictionary<GameObject, List<TimerData>>();

   public void AddTimer(string name, GameObject obj, float timeToFire, string functionToCall, bool shouldRepeat = false)
    {
        TimerData newTimer = new TimerData();
        newTimer.name = name;
        newTimer.timeUntilFired = timeToFire;
        newTimer.fullTimerTime = timeToFire;
        newTimer.gameObject = obj;
        newTimer.functionToCall = functionToCall;
        newTimer.shouldRepeat = shouldRepeat;

        if (!timers.ContainsKey(obj)) { timers[obj] = new List<TimerData>(); }

        timers[obj].Add(newTimer);

        // Extra thing to add:
        // Replace or Ignore based on time with same name already existing.
        // Can be chosen to replace/ignore based on a variable passed in.
    }

    public void RemoveAllObjectTimers(GameObject obj)
    {
        StartCoroutine(DoRemoveAllObjectTimers(obj));
    }

    public void RemoveTimerFromObject(GameObject obj, string name)
    {
        StartCoroutine(DoRemoveTimerFromObject(obj, name));
    }
    	
    public void ChangeTimerLength(GameObject obj, string name, float newTime)
    {
        if (timers.ContainsKey(obj))
        {
            foreach (var timer in timers[obj])
            {
                if (timer.name == name)
                {
                    timer.fullTimerTime = newTime;
                }
            }
        }
    }

	void Update ()
    {
        foreach (var v in timers)
        {
            for(int i = 0; i < v.Value.Count;++i)
            {
                TimerData timer = v.Value[i];

                if (timer.gameObject == null)
                {
                    //return;
                }
                else
                {
                    timer.timeUntilFired -= Time.deltaTime;
                    if (timer.timeUntilFired <= 0.0f)
                    {
                        timer.gameObject.SendMessage(timer.functionToCall, timer.name, SendMessageOptions.RequireReceiver);
                        //Debug.Log("Called " + timer.name + " on " + timer.gameObject);
                        if (!timer.shouldRepeat)
                        {
                            RemoveTimerFromObject(timer.gameObject, timer.name);
                        }

                        else
                        {
                            timer.timeUntilFired = timer.fullTimerTime;
                        }
                    }
                }             
            }
        }
    }
    
    IEnumerator DoRemoveAllObjectTimers(GameObject obj)
    {
        yield return new WaitForEndOfFrame();
        if (timers.ContainsKey(obj))
        {
            timers.Remove(obj);
        }
    }

    IEnumerator DoRemoveTimerFromObject(GameObject obj, string name)
    {
        yield return new WaitForEndOfFrame();
        if (timers.ContainsKey(obj))
        {
            timers[obj].RemoveAll(data => (data.name == name));
            if (timers[obj].Count == 0)
            {
                timers.Remove(obj);
            }
        }
    }
}
