﻿using UnityEngine;
using System.Collections;

public class ScoreControl : MonoBehaviour
{
    ObserverObject observer;

    private float score;

    public int PlantSeedScore;
    public int PlantStageOneScore;
    public int PlantStageTwoScore;
    public int PlantStageThreeScore;
    public int PlantStageFourScore;

    void Start()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();

        observer.AddSubscriber("PlantDestroyed", gameObject, "OnPlantDestroyed");
    }

    private void OnPlantDestroyed(object data)
    {
        PlantPlotObject plantData = data as PlantPlotObject;

        switch(plantData.GetCurrentState())
        {
            case PlantPlotObject.PlantState.SEED:
            {
                score += PlantSeedScore;
                break;
            }

            case PlantPlotObject.PlantState.FIRST_STAGE:
            {
                score += PlantStageOneScore;
                break;
            }

            case PlantPlotObject.PlantState.SECOND_STAGE:
            {
                score += PlantStageTwoScore;
                break;
            }

            case PlantPlotObject.PlantState.THIRD_STAGE:
            {
                score += PlantStageThreeScore;
                break;  
            }
        }

        //Debug.Log("Your current score is: " + score);
        observer.DoEvent("ScoreUpdated", score);
       
    }
}   