﻿using UnityEngine;
using System.Collections;

public class AnnouncerScript : MonoBehaviour
{
    private ObserverObject observer;
    // Use this for initialization
    void Start ()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        
        observer.AddSubscriber("PlantSpawned", gameObject, "AnnounceSpawnedPlant");
        observer.AddSubscriber("PlantDestroyed", gameObject, "AnnounceDestroyedPlant");
        observer.AddSubscriber("ScoreChanged", gameObject, "UpdateScore");

        GameObject.Find("SystemController").GetComponent<GameSystem>().canActive = true;
    }

    private void AnnounceSpawnedPlant(object data)
    {
        //PlantPlotObject newPlant = data as PlantPlotObject;
        
       // Debug.Log("Plant spawned at: " + newPlant.transform.position);
    }

    private void AnnounceDestroyedPlant(object data)
    {
       // PlantPlotObject destroyedPlant = data as PlantPlotObject;
       // Debug.Log("The plant at " + destroyedPlant.transform.position + " was destroyed.");
    }
}