﻿using UnityEngine;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using UnityEngine.UI;
public class ZenGardenControl : MonoBehaviour
{
    public Image backgroundImage;
    public List<Sprite> backgroundImages;
    //public List<PlantIdentity> plants;

    public void Start()
    {
        GameObject.FindObjectOfType<GameSystem>().TeraLoad();
    }

    public void SwitchPanel(int index)
    {
        backgroundImage.sprite = backgroundImages[index];
    }
}