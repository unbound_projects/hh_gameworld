﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    ObserverObject observer;
	public GameObject[] cameraPositions;
	public int camNum = 3;

	void Start ()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        observer.AddSubscriber("CameraMove", gameObject, "CameraMove");
    }

    public void CameraMove(bool junk)
    {
        if (junk)
            camNum += 1;

        else
            camNum -= 1;


        if (camNum <= 0)
        {
            camNum = 4;
        }

        if (camNum >= 5)
        {
            camNum = 1;
        }

 
       switch (camNum)
       {
           case 1:
               {
                   cameraPositions[0].SetActive(true);
                   cameraPositions[1].SetActive(false);
                   cameraPositions[2].SetActive(false);
                   cameraPositions[3].SetActive(false);

                   observer.DoEvent("CameraChange", camNum);
                   break;
               }

           case 2:
               {
                   cameraPositions[0].SetActive(false);
                   cameraPositions[1].SetActive(true);
                   cameraPositions[2].SetActive(false);
                   cameraPositions[3].SetActive(false);

                   observer.DoEvent("CameraChange", camNum);
                   break;
               }

           case 3:
               {
                   cameraPositions[0].SetActive(false);
                   cameraPositions[1].SetActive(false);
                   cameraPositions[2].SetActive(true);
                   cameraPositions[3].SetActive(false);

                   observer.DoEvent("CameraChange", camNum);
                   break;
               }

           case 4:
               {
                   cameraPositions[0].SetActive(false);
                   cameraPositions[1].SetActive(false);
                   cameraPositions[2].SetActive(false);
                   cameraPositions[3].SetActive(true);

                   observer.DoEvent("CameraChange", camNum);
                   break;
               }

       }


    }
}
