﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovement : MonoBehaviour
{
    ObserverObject observer;
    [HideInInspector]
    public int id;

    //public GridSpace gridLoc;    

    [HideInInspector]
    public GameObject currentGrid;

    private float harvestCompletionTime = 0;
    public float actionCompletionTime;


    public float hitCooldown = 0;
    public float hitTime = 0.2f;
    [HideInInspector]
    public Vector3 eventData;

    private bool needsAllignment = false;

    private int allignmentCount = 0;
    private bool levelWon = false;
    private bool levelLoss= false;


    public GameObject winScreen;
    public GameObject loseScreen;

    private bool canCheckLevel = false;

    private InGameMoneyControl playerMoney;
    public LayerMask layerMask;
    void Start ()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        observer.AddSubscriber("PlayerMove", gameObject, "OnActionGiven");
        observer.AddSubscriber("OnHarvest", gameObject, "HarvestPlant");
        observer.AddSubscriber("CameraChange", gameObject, "UpdateCurrentGrid");
        observer.AddSubscriber("EndLevel", gameObject, "EndLevel");

        observer.DoEvent("FindNode", gameObject);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (levelWon|| levelLoss)
        {
            if (levelWon)
            {
                //Can move this to events or something later
                winScreen.SetActive(true);
            }
            else if (levelLoss)
            {
                loseScreen.SetActive(true);
            }

            //Whats happens afterr the game is won/lost?!?
            //GameObject.Find("MainUI").SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            hitCooldown += Time.deltaTime;
            if (Input.GetMouseButtonDown(0) && hitCooldown > hitTime)
            {
                hitTime = 0;
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100,layerMask))
                {
                    eventData = hit.point;
                    if (hit.collider.gameObject.CompareTag("Animal"))
                    {
                        hit.collider.gameObject.GetComponent<EnemyAIControl>().GetHit();
                    }
                    else
                    {
                        hit.collider.gameObject.GetComponent<FarmPlot>().PlotAction();
                    }
                }
            }
        }
    }
       
    /*
    void OnActionGiven(GridSpace gridSpot)
    {
        gridLoc = gridSpot;
        if (gridLoc != null)
        {
            observer.DoEvent("FindNode", gameObject);
            if (gridLoc.resident != null)
            {
                observer.DoEvent("CommenceAction", gameObject);
            }
        }
    }
    */

    public void EndLevel(bool result)
    {
        if (result)
            levelWon = true;
        else
            levelLoss = true;

    }
}