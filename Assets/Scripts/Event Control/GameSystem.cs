﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;

[XmlRoot("LevelHolder")]
public class TexdataLevel
{
    [XmlArray("Levels")]
    [XmlArrayItem("Level")]
    public List<LevelData> levels = new List<LevelData>();

    public static TexdataLevel Load(string path)
    {
        var serializer = new XmlSerializer(typeof(TexdataLevel));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as TexdataLevel;
        }
    }

    //Loads the xml directly from the given string. Useful in combination with www.text.
    public static TexdataLevel LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(TexdataLevel));
        return serializer.Deserialize(new StringReader(text)) as TexdataLevel;
    }

    public void UpdateXML(string dataPath, string defaultPath)
    {
        XmlDocument xmlDoc = new XmlDocument();

        var newLevelsList = levels;

        if (!File.Exists(dataPath))
        {
            WWW reader;
#if UNITY_ANDROID
                reader = new WWW(defaultPath);
#else
            reader = new WWW("file:///" + defaultPath);
#endif
            while (!reader.isDone) { }
            xmlDoc.LoadXml(reader.text);
        }
        else
        {
            xmlDoc.Load(dataPath);
        }

        if (this != null)
        {
            XmlDocument newDoc = new XmlDocument();
            XmlNode rootNode = xmlDoc.CreateElement("LevelHolder");

            // RemoveAll called so that it is simply adding fresh data instead
            // of appending new data which may be more difficult
            rootNode.RemoveAll();

            XmlNode levelsNode = xmlDoc.CreateElement("Levels");
            rootNode.AppendChild(levelsNode);

            for (int i = 0; i < levels.Count; i++)
            {
                XmlNode levelNode = xmlDoc.CreateElement("Level");
                levelsNode.AppendChild(levelNode);

                XmlAttribute levelName = xmlDoc.CreateAttribute("Name");
                levelName.Value = newLevelsList[i].nameOfLevel;

                XmlAttribute levelSeason = xmlDoc.CreateAttribute("Season");
                levelSeason.Value = newLevelsList[i].season.ToString();

                XmlAttribute levelStar = xmlDoc.CreateAttribute("Star");
                levelStar.Value = newLevelsList[i].oneStarPoint.ToString();

                XmlAttribute levelHitStar = xmlDoc.CreateAttribute("HitStar");
                levelHitStar.Value = Convert.ToInt32(newLevelsList[i].starComplete).ToString();

                XmlAttribute levelUnlocked = xmlDoc.CreateAttribute("UnLocked");
                levelUnlocked.Value = Convert.ToInt32(newLevelsList[i].isUnlocked).ToString();


                XmlNode objectivesNode = xmlDoc.CreateElement("Objectives");
                levelNode.AppendChild(objectivesNode);

                for(int j = 0; j < levels[i].objList.Count; ++j)
                {
                    XmlNode objectiveNode = xmlDoc.CreateElement("Objective");
                    objectivesNode.AppendChild(objectiveNode);

                    XmlAttribute objectiveName = xmlDoc.CreateAttribute("name");
                    objectiveName.Value = newLevelsList[i].objList[j].typeName.ToString();

                    XmlAttribute objectiveCount = xmlDoc.CreateAttribute("count");
                    objectiveCount.Value = newLevelsList[i].objList[j].amountToComplete.ToString();

                    XmlAttribute objectiveType = xmlDoc.CreateAttribute("type");
                    objectiveType.Value = newLevelsList[i].objList[j].typeOfTask;

                    XmlAttribute objectiveComplete = xmlDoc.CreateAttribute("complete");
                    objectiveComplete.Value = Convert.ToInt32(newLevelsList[i].objList[j].complete).ToString();

                    objectiveNode.Attributes.Append(objectiveName);
                    objectiveNode.Attributes.Append(objectiveCount);
                    objectiveNode.Attributes.Append(objectiveType);
                    objectiveNode.Attributes.Append(objectiveComplete);

                }

                levelNode.Attributes.Append(levelName);
                levelNode.Attributes.Append(levelSeason);
                levelNode.Attributes.Append(levelStar);
                levelNode.Attributes.Append(levelHitStar);
                levelNode.Attributes.Append(levelUnlocked);

            }

            XmlNode tempAppendNode = newDoc.ImportNode(rootNode, true);
            newDoc.AppendChild(tempAppendNode);
            xmlDoc = newDoc;
            xmlDoc.Save(dataPath);
        }
    }
}

public class LevelData
{
    public enum Seasons
    {
        SUMMER,
        SPRING,
        AUTUMN,
        WINTER,
        NULL
    }
    private enum SeasonalEffects
    {
        TICK_DAMAGE = 1,
    }

    [XmlArray("Objectives")]
    [XmlArrayItem("Objective")]
    public List<ObjectiveCrops> objList = new List<ObjectiveCrops>();

    public List<ObjectiveCrops.PlantDeclare> cropList = new List<ObjectiveCrops.PlantDeclare>();

    public List<ObjectiveCrops.PlantDeclare> allowedCrops = new List<ObjectiveCrops.PlantDeclare>();

    public int rating = 0;
    public int score = 0;
    [XmlAttribute("Name")]
    public string nameOfLevel;
    [XmlAttribute("Season")]
    public Seasons season;

    [XmlAttribute("Star")]
    public int oneStarPoint;

    [XmlAttribute("HitStar")]
    public bool starComplete;

    [XmlAttribute("UnLocked")]
    public bool isUnlocked;

    public int currentHighScore = 0;

    public void CalculateRating()
    {
        if (score >= oneStarPoint)
        {
            rating = 1;
        }
    }

    public int GetRating()
    {
        return rating;
    }

    public void AddScore(int scoreToAdd)
    {
        score += scoreToAdd;
    }

    public Seasons GetSeason()
    {
        return season;
    }
}

//THIS NEEDS TO MOVE INTO ITS OWN SCRIPT PROBS
public abstract class Objective
{
    public enum TaskType
    {
        Crops,
    }

    public int level;
    [XmlAttribute("name")]
    public string typeName;
    [XmlAttribute("count")]
    public int amountToComplete;
    [XmlAttribute("type")]
    public string typeOfTask;
    [XmlAttribute("complete")]
    public bool complete;

    public TaskType task;

    private int currentAmount = 0;

    public void UpdateScore(int amount)
    {
        currentAmount += amount;
        if (currentAmount >= amountToComplete)
        {
            complete = true;
        }
    }

    public void ResetScore()
    {
        currentAmount = 0;
    }

    public int GetScore()
    {
        return currentAmount;
    }

    public ObjectiveCrops.PlantDeclare GetCropType()
    {
        switch (typeName)
        {
            case ("Beetroot"):
                {
                    return ObjectiveCrops.PlantDeclare.Beetroot;
                }
            case ("Pumpkin"):
                {
                    return ObjectiveCrops.PlantDeclare.Pumpkin;
                }
            case ("Turnip"):
                {
                    return ObjectiveCrops.PlantDeclare.Turnip;
                }
            case ("Corn"):
                {
                    return ObjectiveCrops.PlantDeclare.Corn;
                }
            case ("Tomato"):
                {
                    return ObjectiveCrops.PlantDeclare.Tomato;
                }
        }
        return ObjectiveCrops.PlantDeclare.NULL;
    }
    public abstract void PerformStartUp(LevelData entry, int id, List<Sprite> spriteList, GameObject instanceObj);
}

public class ObjectiveCrops : Objective
{
    public enum PlantDeclare
    {
        Beetroot,
        Pumpkin,
        Turnip,
        Corn,
        Tomato,
        NULL
    }
    public PlantDeclare cropType;

    public void SetCropType(LevelData entry, int id)
    {

        switch (entry.objList[id].typeName)
        {
            case ("Beetroot"):
                {
                    cropType = PlantDeclare.Beetroot;
                    break;
                }
            case ("Pumpkin"):
                {
                    cropType = PlantDeclare.Pumpkin;
                    break;
                }
            case ("Turnip"):
                {
                    cropType = PlantDeclare.Turnip;
                    break;
                }
            case ("Corn"):
                {
                    cropType = PlantDeclare.Corn;
                    break;
                }
            case ("Tomato"):
                {
                    cropType = PlantDeclare.Tomato;
                    break;
                }
        }
    }
    public override void PerformStartUp(LevelData entry, int id, List<Sprite> spriteList, GameObject instanceObj)
    {
        GameObject objHolder = GameObject.Find("Objective Holder");
        GameObject cropHolder = GameObject.Find("GameController");

        cropHolder.GetComponent<PlotTypes>().presentCrops = entry.allowedCrops;

        GameObject temp = GameObject.Instantiate(instanceObj, new Vector3(0, 0 - (id * 50), 0), Quaternion.identity) as GameObject;
        temp.transform.GetChild(0).name = "Objective" + (id + 1) + "Image";
        temp.transform.GetChild(1).name = "Objective" + (id + 1) + "Counter";
        temp.transform.GetChild(2).name = "Objective" + (id + 1) + "Score";

        temp.transform.SetParent(objHolder.transform, false);

        Text textTempCount = temp.transform.GetChild(1).GetComponent<Text>();
        Image imgTempIcon = temp.transform.GetChild(0).GetComponent<Image>();
        textTempCount.text = "/ " + entry.objList[id].amountToComplete.ToString();

        switch (entry.objList[id].typeName)
        {
            case ("Beetroot"):
                {
                    cropType = PlantDeclare.Beetroot;
                    break;
                }
            case ("Pumpkin"):
                {
                    cropType = PlantDeclare.Pumpkin;
                    break;
                }
            case ("Turnip"):
                {
                    cropType = PlantDeclare.Turnip;
                    break;
                }
            case ("Corn"):
                {
                    cropType = PlantDeclare.Corn;
                    break;
                }
            case ("Tomato"):
                {
                    cropType = PlantDeclare.Tomato;
                    break;
                }
        }

        temp.name = cropType.ToString() + "Objective";
        imgTempIcon.sprite = GameObject.FindObjectOfType<GameSystem>().GetCropSprite(cropType);
    }

}

public class CropCard
{
    [XmlAttribute("Type")]
    public ObjectiveCrops.PlantDeclare cropType;
}

public class SeedCard
{
    [XmlAttribute("Type")]
    public ObjectiveCrops.PlantDeclare cropType;
}

public class Deck
{
    [XmlAttribute("ID")]
    public int id;

    [XmlArray("CardList")]
    [XmlArrayItem("Card")]
    public List<CropCard> cardList;
}
[XmlRoot("PlayerData")]
public class PlayerData
{
    //SHOULD BE USED TO SAVE CURRENT LEVEL MARK AND GOLD
    [XmlAttribute("Currency")]
    public int currency = 0;
    [XmlAttribute("LevelCap")]
    public int levelCap = 1;

    [XmlArray("LockedCrops")]
    [XmlArrayItem("LockedCrop")]
    public List<TerrariumPlantObject> lockedCrops = new List<TerrariumPlantObject>();
    [XmlArray("UnlockedCrops")]
    [XmlArrayItem("UnlockedCrop")]
    public List<TerrariumPlantObject> unlockedCrops = new List<TerrariumPlantObject>();

    [XmlArray("CardList")]
    [XmlArrayItem("Card")]
    public List<CropCard> cropCardList = new List<CropCard>();

    [XmlArray("DeckList")]
    [XmlArrayItem("Deck")]
    public List<Deck> deckList = new List<Deck>();

    public void ChangeValue(int value)
    {
        currency += value;
    }

    public void ChangeLevelCap(int i)
    {
        levelCap += i;
    }
    public static PlayerData Load(string path)
    {
        var serializer = new XmlSerializer(typeof(PlayerData));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as PlayerData;
        }
    }

    //Loads the xml directly from the given string. Useful in combination with www.text.
    public static PlayerData LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(PlayerData));
        return serializer.Deserialize(new StringReader(text)) as PlayerData;
    }

    public void UpdateXML(string dataPath, string defaultPath)
    {
        XmlDocument xmlDoc = new XmlDocument();

        var newCardList = cropCardList;
        var newDeckList = deckList;

        if (!File.Exists(dataPath))
        {
            WWW reader;
#if UNITY_ANDROID
                reader = new WWW(defaultPath);
#else
            reader = new WWW("file:///" + defaultPath);
#endif
            while (!reader.isDone) { }
            xmlDoc.LoadXml(reader.text);
        }
        else
        {
            xmlDoc.Load(dataPath);
        }

        if (this != null)
        {
            XmlDocument newDoc = new XmlDocument();
            XmlNode rootNode = xmlDoc.CreateElement("PlayerData");

            // RemoveAll called so that it is simply adding fresh data instead
            // of appending new data which may be more difficult
            rootNode.RemoveAll();

            XmlAttribute currency2 = xmlDoc.CreateAttribute("Currency");
            currency2.Value = currency.ToString();
            rootNode.Attributes.Append(currency2);
            XmlAttribute levelcap = xmlDoc.CreateAttribute("LevelCap");
            levelcap.Value = levelCap.ToString();
            rootNode.Attributes.Append(levelcap);
            
            XmlNode lockedCropsNode = xmlDoc.CreateElement("LockedCrops");
            rootNode.AppendChild(lockedCropsNode);

            XmlNode unlockedCropsNode = xmlDoc.CreateElement("UnlockedCrops");
            rootNode.AppendChild(unlockedCropsNode);

            XmlNode cardListNode = xmlDoc.CreateElement("CardList");
            rootNode.AppendChild(cardListNode);

            XmlNode deckListNode = xmlDoc.CreateElement("DeckList");
            rootNode.AppendChild(deckListNode);

            CropSave(xmlDoc, lockedCrops, lockedCropsNode, "LockedCrop");
            CropSave(xmlDoc, unlockedCrops, unlockedCropsNode, "UnlockedCrop");

            //BAD NAMING CONVENTIONS FOR THESE NEXT TWO LOOPS
            for (int i = 0; i < newCardList.Count; i++)
            {
                XmlNode cropNode = xmlDoc.CreateElement("Card");
                cardListNode.AppendChild(cropNode);

                XmlAttribute lockedCropType = xmlDoc.CreateAttribute("Type");
                lockedCropType.Value = newCardList[i].cropType.ToString();

                cropNode.Attributes.Append(lockedCropType);

            }

            for (int i = 0; i < newDeckList.Count; i++)
            {
                XmlNode cropNode = xmlDoc.CreateElement("Deck");
                deckListNode.AppendChild(cropNode);

                XmlAttribute lockedCropType = xmlDoc.CreateAttribute("ID");
                lockedCropType.Value = newDeckList[i].id.ToString();

                XmlNode deckListNode2 = xmlDoc.CreateElement("CardList");
                cropNode.AppendChild(deckListNode2);

                var newCardList2 = newDeckList[i].cardList;

                for (int j = 0; j < newCardList2.Count; ++j)
                {
                    XmlNode lockedCropType2 = xmlDoc.CreateElement("Card");
                    deckListNode2.AppendChild(lockedCropType2);

                    XmlAttribute cropType = xmlDoc.CreateAttribute("Type");
                    cropType.Value = newCardList2[i].cropType.ToString();
                    lockedCropType2.Attributes.Append(cropType);
                }
                cropNode.Attributes.Append(lockedCropType);
            }

            XmlNode tempAppendNode = newDoc.ImportNode(rootNode, true);
            newDoc.AppendChild(tempAppendNode);
            xmlDoc = newDoc;
            xmlDoc.Save(dataPath);
        }
    }

    private void CropSave(XmlDocument xmlDoc, List<TerrariumPlantObject> plantList, XmlNode cropListNode, string elementTitle)
    {
        var newPlantList = plantList;
        for (int i = 0; i < plantList.Count; i++)
        {

            XmlNode cropNode = xmlDoc.CreateElement(elementTitle);
            cropListNode.AppendChild(cropNode);

            XmlAttribute cropType = xmlDoc.CreateAttribute("Type");
            cropType.Value = plantList[i].seedType.ToString();

            XmlAttribute cropCost = xmlDoc.CreateAttribute("Cost");
            cropCost.Value = plantList[i].seedCost.ToString();

            XmlAttribute cropLevel = xmlDoc.CreateAttribute("Level");
            cropLevel.Value = plantList[i].seedLevel.ToString();

            XmlAttribute cropRW = xmlDoc.CreateAttribute("RW");
            cropRW.Value = plantList[i].resistanceWinter.ToString();
            XmlAttribute cropRS = xmlDoc.CreateAttribute("RS");
            cropRS.Value = plantList[i].resistanceSummer.ToString();
            XmlAttribute cropRA = xmlDoc.CreateAttribute("RA");
            cropRA.Value = plantList[i].resistanceAutmn.ToString();
            XmlAttribute cropRSp = xmlDoc.CreateAttribute("RSp");
            cropRSp.Value = plantList[i].resistanceSpring.ToString();
            XmlAttribute favoredSeason = xmlDoc.CreateAttribute("FS");
            favoredSeason.Value = plantList[i].favoredSeason.ToString();
            XmlAttribute unFavoredSeason = xmlDoc.CreateAttribute("UFS");
            unFavoredSeason.Value = plantList[i].unFavoredSeason.ToString();

            cropNode.Attributes.Append(cropType);
            cropNode.Attributes.Append(cropCost);
            cropNode.Attributes.Append(cropLevel);
            cropNode.Attributes.Append(cropRW);
            cropNode.Attributes.Append(cropRS);
            cropNode.Attributes.Append(cropRA);
            cropNode.Attributes.Append(cropRSp);
            cropNode.Attributes.Append(favoredSeason);
            cropNode.Attributes.Append(unFavoredSeason);
        }
    }
    #region Getters/Setters
    public List<TerrariumPlantObject> GetLockedCrops()
    {
        return lockedCrops;
    }

    public List<TerrariumPlantObject> GetUnlockedCrops()
    {
        return unlockedCrops;
    }

    public void UpdateLockedCrops(List<TerrariumPlantObject> updateData)
    {
        lockedCrops = updateData;
    }

    public void UpdateUnlockedCrops(List<TerrariumPlantObject> updateData)
    {
        unlockedCrops = updateData;
    }
    #endregion
}

public class GameSystem : MonoBehaviour
{
    ObserverObject observer;
    TimerObject timer;
    public float secondsToFinish = 60;
    float currentCountDown = 0;
    public LevelData currentLvl = null;
    private Text displayTimer;
    private bool updateLevelInfo = true;

    public List<Text> objScores;
    public bool hasCompletedLevel = false;
    public List<Sprite> spriteList;
    public GameObject objectivePrefab;
    public bool canActive = false;

    private int levelScore;
    private int levelRating;

    LevelData.Seasons levelSeason;

    public List<Text> displayScores = new List<Text>();

    public TextAsset xmlFile;
    public TextAsset playerXml;
    private bool ableToUpdate = true;

    public bool differentUpdateTest = false;

    public PlayerData dataPlayer;
    public TexdataLevel dataLevel;
    public GameObject endCanvas;
    public int savedLevel = -1;

    public List<Canvas> screens = new List<Canvas>();
    public List<TerrariumPlantObject> totalCrops = new List<TerrariumPlantObject>();

    public LevelData.Seasons currentSeason;

    public GameObject plantHolder;
    public List<TerrariumPlantObject> lCrops;
    public List<TerrariumPlantObject> ulCrops;
    public GameObject destroyedResponseText;

    public List<LevelData> summerLevels = new List<LevelData>();
    public List<LevelData> autumnLevels = new List<LevelData>();
    public List<LevelData> winterLevels = new List<LevelData>();
    public List<LevelData> springLevels = new List<LevelData>();

    EnemySpawner[] spawnersInLevel;
    List<EnemySpawner> spawners;

    public float beetMulti;
    public float cornMulti;
    public float turnipMulti;
    public float pumpkinMulti;
    public float tomatoMulti;

    private bool thisIsAPrivateBool = true;
    private bool doEnemyRoutineCheck = false;
    private bool beginEnemySpawning = false;

    GameObject FinalCountdownText;
    GameObject TenSecondText;

    public Sprite highlightedStar;
    public Sprite unhighlightedStar;
    private int prevUnlockedCount = 0;

    [HideInInspector]
    public string dataPlayerDefaultsPath;
    [HideInInspector]
    public string playerSaveDataPath;
    [HideInInspector]
    public string dataLevelPath;
    [HideInInspector]
    public string levelSaveDataPath;
    // Use this for initialization
    public void Awake()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }

        dataPlayerDefaultsPath = System.IO.Path.Combine(Application.streamingAssetsPath, "PlayerData.txt");
        playerSaveDataPath = System.IO.Path.Combine(Application.persistentDataPath, "dataPlayer.dat");
        dataLevelPath = System.IO.Path.Combine(Application.streamingAssetsPath, "LevelElements.txt");
        levelSaveDataPath = System.IO.Path.Combine(Application.persistentDataPath, "dataLevel.dat");

        //If the File path does not exist, use the default values set in the XMLS
        if (!File.Exists(playerSaveDataPath))
        {
            Debug.Log("Loads PlayerData from XML");
            dataPlayer = PlayerData.LoadFromText(playerXml.text);
        }
        else
        {
            Debug.Log("Loads PlayerData from DataPath");
            dataPlayer = PlayerData.Load(playerSaveDataPath);
        }

        if (!File.Exists(levelSaveDataPath))
        {
            dataLevel = TexdataLevel.LoadFromText(xmlFile.text);
        }
        else
        {
            dataLevel = TexdataLevel.Load(levelSaveDataPath);
        }

        lCrops = dataPlayer.GetLockedCrops();
        ulCrops = dataPlayer.GetUnlockedCrops();
                
        for (int i = 0; i < dataLevel.levels.Count; i++)
        {
            if (dataLevel.levels[i].season == LevelData.Seasons.SUMMER)
            {
                summerLevels.Add(dataLevel.levels[i]);
            }
            else if (dataLevel.levels[i].season == LevelData.Seasons.AUTUMN)
            {
                autumnLevels.Add(dataLevel.levels[i]);
            }
            else if (dataLevel.levels[i].season == LevelData.Seasons.WINTER)
            {
                winterLevels.Add(dataLevel.levels[i]);
            }
            else
            {
                springLevels.Add(dataLevel.levels[i]);
            }
        }

        dataLevel.UpdateXML(levelSaveDataPath, dataLevelPath);
    }

    public void TeraLoad()
    {
        lCrops = dataPlayer.GetLockedCrops();
        ulCrops = dataPlayer.GetUnlockedCrops();
        prevUnlockedCount = ulCrops.Count;
        plantHolder = GameObject.FindObjectOfType<CropDetectionTera>().gameObject;
        for (int i = 0; i < ulCrops.Count; ++i)
        {
            for (int j = 0; j < plantHolder.transform.childCount; ++j)
            {
                if (plantHolder.transform.GetChild(j).GetComponent<PlantIdentity>().plantType == ulCrops[i].seedType)
                {
                    plantHolder.transform.GetChild(j).GetComponent<PlantIdentity>().ActivateTexture();
                    break;
                }
            }
        }
    }

    public void LevelStop()
    {
        ableToUpdate = false;
        updateLevelInfo = true;
        canActive = false;


        for (int i = 0; i < currentLvl.objList.Count; ++i)
        {
            currentLvl.objList[i].ResetScore();
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!ableToUpdate && currentLvl != null)
        {
            ableToUpdate = true;
        }
        if (currentLvl != null && canActive && ableToUpdate)
        {
            if (updateLevelInfo)
            {
                levelScore = 0;
                levelRating = 0;
                currentLvl.score = 0;
                Time.timeScale = 1;
                objScores.Clear();
                displayScores.Clear();
                observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
                timer = GameObject.Find("MessageHandler").GetComponent<TimerObject>();
                
                observer.AddSubscriber("PlantDestroyed", gameObject, "ScoreHarvest");
                displayTimer = GameObject.Find("DisplayTimer").GetComponent<Text>();
                displayScores.Add(GameObject.Find("LevelScore").GetComponent<Text>());

                currentCountDown = secondsToFinish;
                levelSeason = currentLvl.GetSeason();
                observer.DoEvent("AssignSeason", levelSeason);

                spawnersInLevel = GameObject.FindObjectsOfType<EnemySpawner>();
                spawners = new List<EnemySpawner>(spawnersInLevel);

                displayTimer.text = 5.ToString();
                for (int i = 0; i < currentLvl.objList.Count; ++i)
                {
                    currentLvl.objList[i].PerformStartUp(currentLvl, i, spriteList, objectivePrefab);
                    objScores.Add(GameObject.Find("Objective" + (i + 1) + "Score").GetComponent<Text>());
                }
                ableToUpdate = false;
                updateLevelInfo = false;
                beginEnemySpawning = true;
            }

            else if (ableToUpdate)
            {
                displayScores[0].text = currentLvl.score.ToString();
                int converted = (int)currentCountDown;
                displayTimer.text = converted.ToString();                

                if (beginEnemySpawning)
                {
                    PrepareSpawnTimer();
                    beginEnemySpawning = false;
                }

                if (currentCountDown > 0)
                {
                    currentCountDown -= Time.deltaTime;

                    if (currentCountDown <= 4.1 && thisIsAPrivateBool)
                    {
                        thisIsAPrivateBool = false; 
                        StartCoroutine(DisplayCountdown());
                    }
                    else if (currentCountDown <= 10 && currentCountDown > 4.1f)
                    {
                        TenSecondText = GameObject.Find("MainUI").transform.FindChild("10SecondAlert").gameObject;
                        FinalCountdownText = GameObject.Find("MainUI").transform.FindChild("FinalCountdown").gameObject;

                        TenSecondText.gameObject.SetActive(true);
                        
                        StartCoroutine(FadeTo(0.0f, 1.0f));
                       
                    }

                }

                // Losing Condition
                else
                {
                    //LOSE STUFF
                    observer.DoEvent("EndLevel", true);
                    Time.timeScale = 0;
 
                    levelScore = currentLvl.score;
                    //savedLevel = currentLvl;
                    //Checks for the correct objective types to add score onto( might need to be refined)
                    LevelStop();
                    
                    endCanvas = FindObjectOfType<PlayerMovement>().winScreen;
                    endCanvas.gameObject.SetActive(true);
                    displayScores.Clear();
                    displayScores.Add(GameObject.Find("BaseScore").GetComponent<Text>());
                    displayScores.Add(GameObject.Find("ExtraCropsScore").GetComponent<Text>());
                    displayScores.Add(GameObject.Find("LevelCompleteScore").GetComponent<Text>());
                    differentUpdateTest = false;
                    // DIS IS WHERE WE DO DA SHIT

                    List<ObjectiveCrops.PlantDeclare> listOfTypes = new List<ObjectiveCrops.PlantDeclare>();
                    float multi = 0;
                    for (int i = 0; i < dataPlayer.deckList[0].cardList.Count; ++i)
                    {
                        bool doesExist = false;
                        for (int j = 0; j < listOfTypes.Count; ++j)
                        {
                            if (listOfTypes[j] == dataPlayer.deckList[0].cardList[i].cropType)
                                doesExist = true;
                        }

                        if (!doesExist)
                        {
                            listOfTypes.Add(dataPlayer.deckList[0].cardList[i].cropType);
                            switch (dataPlayer.deckList[0].cardList[i].cropType)
                            {
                                case (ObjectiveCrops.PlantDeclare.Beetroot):
                                    {
                                        multi += beetMulti;
                                        break;
                                    }
                                case (ObjectiveCrops.PlantDeclare.Corn):
                                    {
                                        multi += cornMulti;
                                        break;
                                    }
                                case (ObjectiveCrops.PlantDeclare.Pumpkin):
                                    {
                                        multi += pumpkinMulti;
                                        break;
                                    }
                                case (ObjectiveCrops.PlantDeclare.Turnip):
                                    {
                                        multi += turnipMulti;
                                        break;
                                    }
                                case (ObjectiveCrops.PlantDeclare.Tomato):
                                    {
                                        multi += tomatoMulti;
                                        break;
                                    }
                            }
                        }
                    }
                    displayScores[0].text = levelScore.ToString();
                    displayScores[1].text = (levelScore * (1 + multi) - levelScore).ToString();
                    levelScore = (int)(levelScore * (1 + multi));
                    displayScores[2].text = levelScore.ToString(); // gotta plus shit
                    
                    if(levelScore > currentLvl.oneStarPoint)
                    {
                        currentLvl.starComplete = true;
                    }
                    dataPlayer.currency += levelScore;

                    Text text1 = GameObject.Find("text1").GetComponent<Text>();
                    Text text2 = GameObject.Find("text2").GetComponent<Text>();
                    Text text3 = GameObject.Find("text3").GetComponent<Text>();
                    Image star1 = GameObject.Find("star1").GetComponent<Image>();
                    Image star2 = GameObject.Find("star2").GetComponent<Image>();
                    Image star3 = GameObject.Find("star3").GetComponent<Image>();

                    text1.text = "Collect " + currentLvl.objList[0].amountToComplete + " " + currentLvl.objList[0].GetCropType().ToString() + "s";
                    if (currentLvl.objList[0].complete)
                    {
                        star1.sprite = highlightedStar;
                    }
                    else
                    {
                        star1.sprite = unhighlightedStar;
                    }

                    text2.text = "Collect " + currentLvl.objList[1].amountToComplete + " " + currentLvl.objList[1].GetCropType().ToString() + "s";
                    if (currentLvl.objList[1].complete)
                    {
                        star2.sprite = highlightedStar;
                    }
                    else
                    {
                        star2.sprite = unhighlightedStar;
                    }
                    text3.text = "Hit a score of " + currentLvl.oneStarPoint.ToString();
                    if (currentLvl.starComplete)
                    {
                        star3.sprite = highlightedStar;
                    }
                    else
                    {
                        star3.sprite = unhighlightedStar;
                    }



                    if (savedLevel < 3)
                    {
                        switch (currentSeason)
                        {
                            case (LevelData.Seasons.SUMMER):
                                {
                                    summerLevels[savedLevel + 1].isUnlocked = true;
                                    break;
                                }
                            case (LevelData.Seasons.AUTUMN):
                                {
                                    autumnLevels[savedLevel + 1].isUnlocked = true;
                                    break;
                                }
                            case (LevelData.Seasons.WINTER):
                                {
                                    winterLevels[savedLevel + 1].isUnlocked = true;
                                    break;
                                }
                            case (LevelData.Seasons.SPRING):
                                {
                                    springLevels[savedLevel + 1].isUnlocked = true;
                                    break;
                                }
                        }
                   }
                    dataLevel.UpdateXML(levelSaveDataPath, dataLevelPath);
                    dataPlayer.UpdateXML(playerSaveDataPath, dataPlayerDefaultsPath);
                    observer.DoEvent("LevelComplete", levelRating - 1);

                    currentLvl = null;
                }
            }
        }
    }

    IEnumerator DisplayCountdown()
    {
        TenSecondText.gameObject.SetActive(true);
        Color newColor = new Color(1, 1, 1, 1);
        TenSecondText.transform.GetComponent<Renderer>().material.color = newColor; 
        int iCounter = 3;
        while (iCounter > 0)
        {

            TenSecondText.GetComponent<TextMesh>().text = iCounter + " seconds remaining";
            yield return new WaitForSeconds(1.3f);
            iCounter--;
        }
        TenSecondText.transform.GetComponent<Renderer>().material.color = new Color(0,0,0,0);
    }

    public void SetLevel(LevelData level)
    {
       
        currentLvl = level;
    }

    public void ScoreHarvest(PlantPlotObject crop)
    {
        if (ableToUpdate)
        {
            int scoreAdd = 0;
            switch (crop.GetCurrentState())
            {
                case PlantPlotObject.PlantState.SEED:
                    {
                        scoreAdd = 0;
                        break;
                    }

                case PlantPlotObject.PlantState.FIRST_STAGE:
                    {
                        scoreAdd = 1;
                        break;
                    }

                case PlantPlotObject.PlantState.SECOND_STAGE:
                    {
                        scoreAdd = 2;
                        break;
                    }

                case PlantPlotObject.PlantState.THIRD_STAGE:
                    {
                        // Third stage = mutation stage
                        scoreAdd = 0;
                        break;
                    }
            }
            GameObject item = Instantiate(destroyedResponseText, crop.gameObject.transform.position, destroyedResponseText.transform.rotation) as GameObject;
            item.GetComponent<TextMesh>().text = "+" + scoreAdd.ToString();
            currentLvl.AddScore(scoreAdd);
            ;
            //Checks for the correct objective types to add score onto( might need to be refined)
            for (int i = 0; i < currentLvl.objList.Count; ++i)
            {
                if (currentLvl.objList[i].cropType == crop.plantType)
                {
                    currentLvl.objList[i].UpdateScore(scoreAdd);
                    objScores[i].text = currentLvl.objList[i].GetScore().ToString();
                }
            }
        }
    }

    //get id for current level
    public Vector3 GetStarValue()
    {
        Vector3 value = new Vector3();
        List<LevelData> tempData = dataLevel.levels;
        value.x = currentLvl.oneStarPoint;
        return value;
    }
    
    //get current levels name
    public string GetNameOfLevel()
    {
        return currentLvl.nameOfLevel;
    }
    //Get id'd levels name
    public string GetNameOfLevel(int id)
    {
        return dataLevel.levels[id].nameOfLevel;
    }
    public Sprite GetCropSprite(ObjectiveCrops.PlantDeclare cropType)
    {
        int id = 0;
        switch (cropType)
        {
            case (ObjectiveCrops.PlantDeclare.Beetroot):
                id = 0;
                break;
            case (ObjectiveCrops.PlantDeclare.Pumpkin):
                id = 1;
                break;
            case (ObjectiveCrops.PlantDeclare.Turnip):
                id = 2;
                break;
            case (ObjectiveCrops.PlantDeclare.Corn):
                id = 3;
                break;
            case (ObjectiveCrops.PlantDeclare.Tomato):
                id = 4;
                break;
            case (ObjectiveCrops.PlantDeclare.NULL):
                Debug.Assert(true, "Invalid Crop passed in");
                break;

        }
        return spriteList[id];
    }
    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = TenSecondText.transform.GetComponent<Renderer>().material.color.a;

        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t) * 4 );
            TenSecondText.transform.GetComponent<Renderer>().material.color = newColor;
            yield return null;
        }
        
    }

    void PrepareSpawnTimer()
    {
        timer.AddTimer("SpawnEnemy", this.gameObject, 5.0f, "DoEnemySpawnCheck", true);
    }

    void DoEnemySpawnCheck()
    {
        EnemyAIControl[] enemies = GameObject.FindObjectsOfType<EnemyAIControl>();
        List<EnemyAIControl> listOfEnemies = new List<EnemyAIControl>(enemies);

        if (listOfEnemies.Count <= spawners.Count /* change this to a max # of enemies variable */)
        {
            int randomSpawnerValue = UnityEngine.Random.Range(0, spawners.Count);
                    
            for (int i = 0; i < randomSpawnerValue; i++)
            {
                spawners[i].canSpawn = true;
            }            
        }
    }
}