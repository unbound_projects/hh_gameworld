﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class AnimateUV : MonoBehaviour {

    public float scrollSpeed = .5f;
    public float offset = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        offset += (Time.deltaTime * scrollSpeed) / 10.0f;
        gameObject.GetComponent<Image>().material.SetTextureOffset("_MainTex", new Vector2(0, offset));

    }
}
