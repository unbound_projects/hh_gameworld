﻿using UnityEngine;
using System.Collections;

public class ButtonInGame : MonoBehaviour {

    public GameObject pausePanel;
    public GameObject player;
	// Use this for initialization
	void Start () {
        player = GameObject.FindObjectOfType<PlayerMovement>().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PauseGame()
    {
        if (Time.timeScale == 0)
        {
            pausePanel.SetActive(false);
            player.SetActive(true);
            Time.timeScale = 1;
        }
        else
        {
            pausePanel.SetActive(true);
            player.SetActive(false);
            Time.timeScale = 0;
        }
    }
    public void ExitGame()
    {
        GameObject.FindObjectOfType<GameSystem>().LevelStop();
        GameObject.FindObjectOfType<GameSystem>().currentLvl = null;
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
