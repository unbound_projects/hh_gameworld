﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class SelectSeason : MonoBehaviour {

    public int seasonID;
    public GameObject levelObject;
    public List<Sprite> seasonSprites;
    public List<Sprite> levelSprites;
    public List<Sprite> particleSprites;
    public List<Sprite> levelSelectSprites;

    private GameSystem gameSys;
    public GameObject levelHolder;
    public Image seasonImage;
    public Image levelImage;
    public Image particleImage;
    public Image levelSelectImage;
    // Use this for initialization
    void Start ()
    {
        gameSys = GameObject.Find("SystemController").GetComponent<GameSystem>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void SeasonLock()
    {
        //Gotta do Season ID aswell
        levelObject.SetActive(true);
        GameObject.Find("SeasonSelect").gameObject.SetActive(false);


        /*
         * SUMMER = 0
         * SPRING = 1
         * FALL = 2
         * WINTER = 3
         */ 
        gameSys.currentSeason = (LevelData.Seasons)seasonID;
        levelHolder.GetComponent<FloatingIsland>().RefreshVisual();

    }

    public void ChangeSeason(int changeValue)
    {
        seasonID += changeValue;
        if (seasonID < 0)
        {
            seasonID = 3;
        }
        else if (seasonID > 3)
        {
            seasonID = 0;
        }

        levelImage.sprite = levelSprites[seasonID];
        particleImage.sprite = particleSprites[seasonID];
        seasonImage.sprite = seasonSprites[seasonID];
        levelSelectImage.sprite = levelSelectSprites[seasonID];
    }
}
