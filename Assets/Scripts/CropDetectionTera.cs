﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class CropDetectionTera : MonoBehaviour {

    public GameObject popUpWindow;
    public Image spriteHolder;
    public Image costFill;
    public Text costObject;
    public Text nameObject;
    public ObserverObject observer;

    public bool isPlantShop = true;
    private GameSystem system;

    public Image backgroundImage;
    public List<Sprite> backgroundImages;
    public List<LevelData.Seasons> seasonList = new List<LevelData.Seasons>();
    public int cropIndex = 0;
    public void Start()
    {
        system = GameObject.FindObjectOfType<GameSystem>();
        observer = GameObject.FindObjectOfType<ObserverObject>();
        observer.AddSubscriber("PurchasedCrop", gameObject, "ChangeCrops");
        ChangeCrops();
    }
    public void OnHit(int id)
    {
        PlantIdentity plantIndent = transform.GetChild(id).GetComponent<PlantIdentity>();
        GameSystem system = GameObject.FindObjectOfType<GameSystem>();
        if (plantIndent.isPlant)
        {
            if (plantIndent.purchased == false)
            {
                for (int i = 0; i < system.lCrops.Count; ++i)
                {
                    if (system.lCrops[i].seedType == plantIndent.plantType)
                    {
                        if (system.dataPlayer.currency >= system.lCrops[i].seedCost)
                        {
                            system.dataPlayer.currency -= system.lCrops[i].seedCost;
                            system.ulCrops.Add(system.lCrops[i]);
                            system.lCrops.Remove(system.lCrops[i]);
                            observer.DoEvent("PurchasedCrop", null);

                            //system.dataPlayer.UpdateXML(system.playerSaveDataPath, system.dataPlayerDefaultsPath);
                           
                        }
                        /*
                        popUpWindow.SetActive(true);
                        popUpWindow.GetComponent<PopUpTera>().LockedCropData(i);
                        nameObject.text = system.lCrops[i].seedType.ToString();
                        costObject.text = "$ " + system.lCrops[i].seedCost.ToString();
                        spriteHolder.sprite = system.GetCropSprite(plantIndent.plantType);

                        costFill.fillAmount = (float)system.dataPlayer.currency / (float)system.lCrops[i].seedCost;*/
                    }
                }
            }
        }
        else
        {
            if (system.dataPlayer.currency >= 100)
            {
                switch (plantIndent.seasonType)
                {
                    case (LevelData.Seasons.AUTUMN):
                        {
                            system.autumnLevels[0].isUnlocked = true;
                            break;
                        }
                    case (LevelData.Seasons.SPRING):
                        {
                            system.springLevels[0].isUnlocked = true;
                            break;
                        }
                    case (LevelData.Seasons.SUMMER):
                        {
                            system.summerLevels[0].isUnlocked = true;
                            break;
                        }
                    case (LevelData.Seasons.WINTER):
                        {
                            system.winterLevels[0].isUnlocked = true;
                            break;
                        }
                }
                system.dataPlayer.currency -= 100;
                observer.DoEvent("PurchasedCrop", null);
            }

        }
        ChangeCrops();
        system.dataPlayer.UpdateXML(system.playerSaveDataPath, system.dataPlayerDefaultsPath);
        system.dataLevel.UpdateXML(system.levelSaveDataPath, system.dataLevelPath);
    }

    public void SwitchShopType(int index)
    {
        backgroundImage.sprite = backgroundImages[index];


        if (index == 0)
        {
            isPlantShop = true;
            ChangeCrops();
        }
        else
        {
            isPlantShop = false;
            ChangeCrops();
        }

    }

    public void ChangeCrops()
    {
        for (int i = 0; i < 6; ++i)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        if (isPlantShop)
        {
            for (int i = 0; i < 6; ++i)
            {
                transform.GetChild(i).GetComponent<PlantIdentity>().isPlant = true;
                if (system.lCrops.Count > cropIndex + i)
                {
                    transform.GetChild(i).GetComponent<PlantIdentity>().plantType = system.lCrops[cropIndex + i].seedType;
                    transform.GetChild(i).GetChild(1).GetComponent<Text>().text = system.lCrops[cropIndex + i].seedCost.ToString();
                }
                else
                {
                    transform.GetChild(i).GetComponent<PlantIdentity>().plantType = ObjectiveCrops.PlantDeclare.NULL;
                }

                transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = system.GetCropSprite(transform.GetChild(i).GetComponent<PlantIdentity>().plantType);
                transform.GetChild(i).GetComponent<PlantIdentity>().ChangedType();
            }
        }
        else
        {
            seasonList.Clear();

            if (!system.summerLevels[0].isUnlocked)
            {
                seasonList.Add(LevelData.Seasons.SUMMER);
            }
            if (!system.autumnLevels[0].isUnlocked)
            {
                seasonList.Add(LevelData.Seasons.AUTUMN);
            }
            if (!system.springLevels[0].isUnlocked)
            {
                seasonList.Add(LevelData.Seasons.SPRING);
            }
            if (!system.winterLevels[0].isUnlocked)
            {
                seasonList.Add(LevelData.Seasons.WINTER);
            }

            for (int i = 0; i < seasonList.Count; ++i)
            {
                transform.GetChild(i).GetChild(1).GetComponent<Text>().text = "100";
                transform.GetChild(i).gameObject.SetActive(true);
                transform.GetChild(i).gameObject.GetComponent<PlantIdentity>().isPlant = false;
                transform.GetChild(i).gameObject.GetComponent<PlantIdentity>().seasonType = seasonList[i];
            }
            /*
               transform.GetChild(i).GetComponent<PlantIdentity>().plantType = system.lCrops[cropIndex + i].seedType;
               transform.GetChild(i).GetChild(1).GetComponent<Text>().text = system.lCrops[cropIndex + i].seedCost.ToString();
           }
           else
           {
               transform.GetChild(i).GetComponent<PlantIdentity>().plantType = ObjectiveCrops.PlantDeclare.NULL;
           }

           transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = system.GetCropSprite(transform.GetChild(i).GetComponent<PlantIdentity>().plantType);
           transform.GetChild(i).GetComponent<PlantIdentity>().ChangedType();
            
    */

        }
    }
    public void IndexChange(int amount)
    {
        cropIndex += amount;
    }
}
