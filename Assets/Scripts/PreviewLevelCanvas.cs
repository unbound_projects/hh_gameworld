﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PreviewLevelCanvas : MonoBehaviour {

    public int id;
    public GameObject previousCanvas;
    public GameObject levelSelectCanvas;
    public GameObject warningSign;
    private GameSystem system;
    public Text nameofLevel;
    public Text text1;
    public Text text2;
    public Text text3;
    public Image star1;
    public Image star2;
    public Image star3;

    public Sprite highlightedStar;
    public Sprite unhighlightedStar;

    public List<GameObject> cropButtons;
    public GameObject objectiveButton;
    public GameObject objectiveButton2;
    public GameObject deckCanvas;

    public LevelData currentLevel = new LevelData();

    // Use this for initialization
    void OnEnable ()
    {
        system = GameObject.FindObjectOfType<GameSystem>();
        levelSelectCanvas.SetActive(false);
        Reset();

        system.savedLevel = id;

        //currentLevel.objList[0].SetCropType(currentLevel, 0);
        //currentLevel.objList[1].SetCropType(currentLevel, 1);
        //star Value
        text1.text = "Collect " + currentLevel.objList[0].amountToComplete + " " + currentLevel.objList[0].GetCropType().ToString() + "s";
        if(currentLevel.objList[0].complete)
        {
            star1.sprite = highlightedStar;
        }
        else
        {
            star1.sprite = unhighlightedStar;
        }

        text2.text = "Collect " + currentLevel.objList[1].amountToComplete + " " + currentLevel.objList[1].GetCropType().ToString() + "s";
        if (currentLevel.objList[1].complete)
        {
            star2.sprite = highlightedStar;
        }
        else
        {
            star2.sprite = unhighlightedStar;
        }
        text3.text = "Hit a score of " +  currentLevel.oneStarPoint.ToString();
        if (currentLevel.starComplete)
        {
            star3.sprite = highlightedStar;
        }
        else
        {
            star3.sprite = unhighlightedStar;
        }

        nameofLevel.text = currentLevel.nameOfLevel;
        //Display
        for (int i = 0; i < cropButtons.Count; ++i)
        {
            cropButtons[i].SetActive(false);
        }
        for (int i = 0; i < system.dataPlayer.deckList[0].cardList.Count; ++i)
        {
            cropButtons[i].GetComponent<Image>().sprite = system.GetCropSprite(system.dataPlayer.deckList[0].cardList[i].cropType);
            cropButtons[i].SetActive(true);
        }
        objectiveButton.GetComponent<Image>().sprite = system.GetCropSprite(currentLevel.objList[0].GetCropType());
        objectiveButton2.GetComponent<Image>().sprite = system.GetCropSprite(currentLevel.objList[1].GetCropType());

        DeckToObjective();
    }
    void Update()
    {
        RefreshDeck();
    }
    public void OnBack()
    {
        previousCanvas.SetActive(true);
        gameObject.SetActive(false);
    }
    public void Reset()
    {
        for (int i = 0; i < cropButtons.Count; ++i)
        {
            cropButtons[i].GetComponent<Image>().sprite = null;
        }
    }
    public void OnPlay()
    {
        List<ObjectiveCrops.PlantDeclare> tempCropList = new List<ObjectiveCrops.PlantDeclare>();
        for (int i = 0; i < system.dataPlayer.deckList[0].cardList.Count; ++i)
        {
              tempCropList.Add(system.dataPlayer.deckList[0].cardList[i].cropType);
        }
        currentLevel.allowedCrops = tempCropList;
        levelSelectCanvas.GetComponent<MainMenuControl>().LoadGameWorld(currentLevel);
    }

    public void DeckToObjective()
    {
        bool obj1Match = false;
        bool obj2Match = false;
        for (int i = 0; i < system.dataPlayer.GetUnlockedCrops().Count; ++i)
        {
            if(system.dataPlayer.GetUnlockedCrops()[i].seedType == currentLevel.objList[0].GetCropType())
            {
                obj1Match = true;
            }
            if (system.dataPlayer.GetUnlockedCrops()[i].seedType == currentLevel.objList[1].GetCropType())
            {
                obj2Match = true;
            }

        }
        system.dataPlayer.deckList[0].cardList.Clear();

        for(int i = 0; i < 4; ++i)
        {
            CropCard tempCrop = new CropCard();
            if(obj1Match && obj2Match)
            {
                if (i < 2)
                {
                    tempCrop.cropType = currentLevel.objList[0].GetCropType();
                    cropButtons[i].GetComponent<Image>().sprite = system.GetCropSprite(currentLevel.objList[0].GetCropType());
                    cropButtons[i].SetActive(true);
                }
                else
                {
                    tempCrop.cropType = currentLevel.objList[1].GetCropType();
                    cropButtons[i].GetComponent<Image>().sprite = system.GetCropSprite(currentLevel.objList[1].GetCropType());
                    cropButtons[i].SetActive(true);
                }
            }
            else if(obj1Match)
            {
                tempCrop.cropType = currentLevel.objList[0].GetCropType();
                cropButtons[i].GetComponent<Image>().sprite = system.GetCropSprite(currentLevel.objList[0].GetCropType());
                cropButtons[i].SetActive(true);
            }
            else if(obj2Match)
            {
                tempCrop.cropType = currentLevel.objList[1].GetCropType();
                cropButtons[i].GetComponent<Image>().sprite = system.GetCropSprite(currentLevel.objList[1].GetCropType());
                cropButtons[i].SetActive(true);
            }

            system.dataPlayer.deckList[0].cardList.Add(tempCrop);

        }
        RefreshDeck();
    }

    public void SwitchDeckChange()
    {
        deckCanvas.SetActive(!deckCanvas.activeSelf);
    }

    public void RefreshDeck()
    {
        for (int i = 0; i < 4; ++i)
        {
            if (i < system.dataPlayer.deckList[0].cardList.Count)
            {
                cropButtons[i].GetComponent<Image>().sprite = system.GetCropSprite(system.dataPlayer.deckList[0].cardList[i].cropType);
                cropButtons[i].SetActive(true);
            }
        }
    }
}
