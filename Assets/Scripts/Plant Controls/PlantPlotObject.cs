﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;
using System;

public class PlantPlotObject : PlotObject
{
    public enum PlantState
    {
        SEED,
        FIRST_STAGE,
        SECOND_STAGE,
        THIRD_STAGE,
    }

    [HideInInspector]
    public ObjectiveCrops.PlantDeclare plantType;

    private bool passiveWaitUntilDestroyed = false;

    public LevelData.Seasons favouredSeason;
    public LevelData.Seasons unfavouredSeason;
    LevelData.Seasons oppositeSeason;
    
    public GameObject feedbackImage;

    private PlantState currentState = 0;

    public GameObject mutatedModel;
    public float health;

    public GameObject gibs;
    private int numberOfGibs = 5;
    private int numberOfGibs2 = 10;
    private int numberOfGibs3 = 20;
    private float gibExplosionNum = 0.05f;

    private bool canEvolve = true;
    private bool hasGrubEffect = false;

    [HideInInspector]
    public bool hasSeasonalTimer = false;

    [HideInInspector]
    public bool hasEffectApplied = false;

    private bool isInFavouredSeason;

    private ObserverObject observer;
    private TimerObject timer;
    MutationControl mtControl;
    private SeasonalSystem seasonalSys;

    public GameObject destroyedResponseText;

    public GameObject growthIndicator;
    float evolveTime = 0;
    float currentTimer = 0;

    public PlantState GetCurrentState() { return currentState; }

    void Start()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        timer = GameObject.Find("MessageHandler").GetComponent<TimerObject>();
        seasonalSys = GameObject.Find("SeasonalHandler").GetComponent<SeasonalSystem>();

        observer.AddSubscriber("OnHarvest", gameObject, "OnPlayerStartedHarvesting");
        observer.AddSubscriber("ApplySeasonalEffectTimer", gameObject, "ApplySeasonalTimer");
        observer.AddSubscriber("SeasonalTick", gameObject, "ApplySeasonalDamage");
        observer.AddSubscriber("ApplyGrubEffect", gameObject, "ApplyGrubEffect");

        currentState = PlantState.SEED;
        transform.localPosition = new Vector3(0, 0, 0);

        if (favouredSeason == LevelData.Seasons.SUMMER)
        {
            oppositeSeason = LevelData.Seasons.WINTER;
        }
        else if (favouredSeason == LevelData.Seasons.WINTER)
        {
            oppositeSeason = LevelData.Seasons.SUMMER;
        }

        if (favouredSeason == unfavouredSeason || unfavouredSeason == favouredSeason)
        {
            unfavouredSeason = oppositeSeason;
        }
        observer.DoEvent("PlantSpawned", this);

        OnStateChange();
        PrepareToEvolve();

        growthIndicator.GetComponent<Renderer>().material.SetFloat("_FillAmount", 0);
    }

    void Update()
    {
        if (health <= 0)
        {
            OnHealthZero();
        }
        currentTimer += Time.deltaTime;
        growthIndicator.GetComponent<Renderer>().material.SetFloat("_FillAmount", (currentTimer / (evolveTime * 2)));
    }

    private void DoPlantPassiveEffect()
    {
        switch (plantType)
        {
            case ObjectiveCrops.PlantDeclare.Beetroot:
                {
                    //Beetroot passive effect?

                    break;
                }

            case ObjectiveCrops.PlantDeclare.Corn:
                {
                    // Corn passive effect? Should wait until dead?
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Pumpkin:
                {
                    // As above
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Turnip:
                {
                    // As above
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Tomato:
            {
                // As above
                break;
            }

            default:
                {
                    break;
                }
        }
    }

    private void PrepareToEvolve()
    {
        switch (plantType)
        {
            case ObjectiveCrops.PlantDeclare.Beetroot:
                {
                    evolveTime = UnityEngine.Random.Range(3f, 4f);
                    timer.AddTimer("EvolvePlant", gameObject, evolveTime, "AdvancePlantStage", true);
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Corn:
                {
                    evolveTime = UnityEngine.Random.Range(3.5f, 4.5f);
                    timer.AddTimer("EvolvePlant", gameObject, evolveTime, "AdvancePlantStage", true);
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Pumpkin:
                {
                     evolveTime = UnityEngine.Random.Range(5f, 7f);
                    timer.AddTimer("EvolvePlant", gameObject, evolveTime, "AdvancePlantStage", true);
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Turnip:
                {
                     evolveTime = UnityEngine.Random.Range(3f, 5f);
                    timer.AddTimer("EvolvePlant", gameObject, evolveTime, "AdvancePlantStage", true);
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Tomato:
                {
                     evolveTime = UnityEngine.Random.Range(2f, 3f);
                    timer.AddTimer("EvolvePlant", gameObject, evolveTime, "AdvancePlantStage", true);
                    break;
                }

            default:
                {
                    break;
                }
        }

        //timer.AddTimer("EvolvePlant", gameObject, evolveTime, "AdvancePlantStage", true);

        /* if (!isInFavouredSeason)
        {
            timer.ChangeTimerLength(this.gameObject, "EvolvePlant", unfavouredEvolveTime);
        }*/
    }

    private void AdvancePlantStage()
    {
        if (canEvolve)
        {
            currentState += 1;
            OnStateChange();
        }
    }

    public override void OnStateChange()
    {
        float timesValue = 1.0f;
        SetMeshFalse();
        switch (currentState)
        {
            case PlantState.SEED:
                {
                    transform.GetChild(0).gameObject.SetActive(true);

                    break;
                }

            case PlantState.FIRST_STAGE:
                {
                    transform.GetChild(1).gameObject.SetActive(true);
                    timesValue = 0.5f;

                    break;
                }

            case PlantState.SECOND_STAGE:
                {
                    transform.GetChild(2).gameObject.SetActive(true);
                    timesValue = 0.25f;
                    break;
                }

            //MUTANT STAGE
            case PlantState.THIRD_STAGE:
                {
                    transform.GetChild(3).gameObject.SetActive(true);
                    mtControl = transform.GetChild(3).gameObject.GetComponent<MutationControl>();

                    mtControl.pType = plantType;
                    canEvolve = false;
                    timer.RemoveTimerFromObject(this.gameObject, "EvolvePlant");
                    growthIndicator.SetActive(false);
                    break;
                }
        }
        for (int cnt = 0; cnt <= numberOfGibs; cnt++)
        {
            Instantiate(gibs, transform.position + (UnityEngine.Random.insideUnitSphere * gibExplosionNum * timesValue), Quaternion.identity);
        }
    }

    public override void OnDestroyed()
    {
        observer.DoEvent("PlantDestroyed", this);
        GameObject.FindObjectOfType<SeedPouch>().AddSeed(1);

        timer.RemoveTimerFromObject(this.gameObject, "EvolvePlant");
        timer.RemoveTimerFromObject(this.gameObject, "SeasonalDamageTimer");

        if (currentState != PlantState.SEED && currentState != PlantState.THIRD_STAGE)
        {
            GameObject instantiatedIcon = Instantiate(feedbackImage, new Vector3(transform.position.x, transform.position.y + 0.75f, transform.position.z), feedbackImage.transform.rotation) as GameObject;
            instantiatedIcon.GetComponent<FeedbackScript>().parentObject = this.gameObject;
        }

        for (int cnt = 0; cnt <= numberOfGibs2; cnt++)
        {
            Instantiate(gibs, transform.position + (UnityEngine.Random.insideUnitSphere * gibExplosionNum), Quaternion.identity);
        }

        Destroy(this.gameObject);
    }

    public void OnHealthZero()
    {
        timer.RemoveTimerFromObject(this.gameObject, "EvolvePlant");
        timer.RemoveTimerFromObject(this.gameObject, "SeasonalDamageTimer");

        for (int cnt = 0; cnt <= numberOfGibs2; cnt++)
        {
            Instantiate(gibs, transform.position + (UnityEngine.Random.insideUnitSphere * gibExplosionNum), Quaternion.identity);
        }

        Destroy(this.gameObject);
    }

    private void OnPlayerStartedHarvesting(object obj)
    {
        if (obj != null)
        {
            if ((obj as PlantPlotObject) == this)
            {
                canEvolve = false;
            }
        }
    }

    private void SetMeshFalse()
    {
        for (int i = 0; i < transform.childCount - 1; ++i)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private void ApplySeasonalDamage()
    {
        health -= seasonalSys.GetDamage();
    }

    private void ApplyGrubEffect()
    {

    }

    private void ApplySeasonalTimer(float damageTimeInterval)
    {
        timer.AddTimer("SeasonalDamageTimer", gameObject, damageTimeInterval, "ApplySeasonalDamage", true);

        if (health > 0)
        {
            hasSeasonalTimer = true;
        }
    }
}