﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MutationControl : MonoBehaviour
{
    private ObserverObject observer;

    [HideInInspector]
    public ObjectiveCrops.PlantDeclare pType;

    [HideInInspector]
    public FarmPlot[] plots;

    private List<FarmPlot> allPlots;
    private GameObject singleTarget = null;
    private List<GameObject> targets;

    [HideInInspector]
    public bool isMutated = false;

    [HideInInspector]
    public bool isAlive = true;
    
    bool isAttacking = true;
    bool canUpdateTarget = true;
    public Vector2 localSquareArea;
    bool shouldAttack = false;
    
    public float counterTimer = 0;
    public float delayTimer = 5f;

    private Animator anim;

    // Finds a random filled plot & deals a big burst of damage to the plant
    List<FarmPlot> plotsWithinRange;

    public float attackRange;
    public int damage;
 
    void Start()
    {
        //observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        //observer.AddSubscriber("PlantMutated", gameObject, "ActivateMutation");

        plots = GameObject.FindObjectsOfType<FarmPlot>();
        allPlots = new List<FarmPlot>(plots);
        allPlots.Remove(gameObject.transform.parent.transform.parent.GetComponent<FarmPlot>());

        plotsWithinRange = new List<FarmPlot>();
        targets = new List<GameObject>();
    }
    
    void Update()
    {
        if(counterTimer > delayTimer)
        {
            if(singleTarget != null)
            {
                HandleMutation(pType);
            }
            else
            {
                if (pType == ObjectiveCrops.PlantDeclare.Beetroot || pType == ObjectiveCrops.PlantDeclare.Turnip)
                {
                    FindTarget();
                }

                if (pType == ObjectiveCrops.PlantDeclare.Pumpkin || pType == ObjectiveCrops.PlantDeclare.Corn || pType == ObjectiveCrops.PlantDeclare.Tomato)
                {
                    FindTarget();
                }
            }

            counterTimer = 0;
        }
        counterTimer += Time.deltaTime;
    }
    void HandleMutation(ObjectiveCrops.PlantDeclare plantType)
    {
        switch (pType)
        {
            case ObjectiveCrops.PlantDeclare.Beetroot:
                {
                    BeetrootMutationEffect();
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Pumpkin:
                {
                    PumpkinMutationEffect();
                    break;
                }

            case ObjectiveCrops.PlantDeclare.Turnip:
                {
                    TurnipMutationEffect();
                    break;
                }
            case ObjectiveCrops.PlantDeclare.Corn:
                {
                    CornMutationEffect();
                    break;
                }
            case ObjectiveCrops.PlantDeclare.Tomato:
                {
                    CornMutationEffect();
                    break;
                }
        }
    }

    void BeetrootMutationEffect()
    {
        singleTarget.GetComponent<PlantPlotObject>().health -= damage;
    }
    void PumpkinMutationEffect()
    {
        if (plotsWithinRange.Count > 0)
        {
            foreach (GameObject target in targets)
            {
                target.GetComponent<PlantPlotObject>().health -= damage;
            }
        }

        else if (plotsWithinRange.Count <= 0)
        {
            return;
        }

    }

    void TurnipMutationEffect()
    {
        BeetrootMutationEffect();
    }
    void CornMutationEffect()
    {

    }
    void FindTarget()
    {
        Vector3 myPos = transform.position;

        for (int i = 0; i < allPlots.Count; i++)
        {
            float distance = (allPlots[i].gameObject.transform.position - myPos).magnitude;

            if (distance <= attackRange)
            {
                plotsWithinRange.Add(allPlots[i]);
            }
        }

        if (plotsWithinRange.Count != -1)
        {
            int random = Random.Range(0, plotsWithinRange.Count);
            singleTarget = plotsWithinRange[random].cropChild;
        }
    }
    
    void FindLocalPlots()
    {
        Vector3 myPos = transform.position;

        for (int i = 0; i < allPlots.Count; i++)
        {
            float distance = (allPlots[i].gameObject.transform.position - myPos).magnitude;

            if (distance <= localSquareArea.x || distance <= localSquareArea.y)
            {
                plotsWithinRange.Add(allPlots[i]);
            }
        }        

        if (plotsWithinRange.Count > 0)
        {
            for (int i = 0; i < plotsWithinRange.Count; i++)
            {
                targets.Add(plotsWithinRange[i].cropChild);
            }
        }
    }

    void ActivateMutation()
    {
        shouldAttack = true;
    }
}