﻿using UnityEngine;
using System.Collections;

public abstract class PlotObject : MonoBehaviour 
{
    // VARIABLES
    private FarmPlot owningPlot;
    
    // FUNCTIONS
    public void SetOwningPlot(FarmPlot plot) { owningPlot = plot; }

    //public abstract void OnCreated();
    public abstract void OnStateChange();
    public virtual void OnDestroyed() { }

    public void Destroy() { OnDestroyed(); owningPlot.OnPlotObjectDestroyed(); }
}
