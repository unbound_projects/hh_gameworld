﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FarmPlot : MonoBehaviour
{
   ObserverObject observer;
    public GameObject cropChild;
    public bool isCrop;
    void Start ()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        observer.AddSubscriber("PlotClicked", gameObject, "PlotAction");
        observer.AddSubscriber("HarvestComplete", gameObject, "DestroyPlant");
     
        //currentObject = null;
        cropChild = null;
    }

    private void Spawn()
    {

        PlotTypes types = GameObject.Find("GameController").GetComponent<PlotTypes>();

        //Inset Randomization Code for the plants k thx
       

        int randVal = Random.Range(0, types.presentCrops.Count);
        //end randomization code
        if (GameObject.FindObjectOfType<SeedPouch>().GetCropToPlace() == ObjectiveCrops.PlantDeclare.NULL)
        {
            return;
        }
        ObjectiveCrops.PlantDeclare tempCropType = (ObjectiveCrops.PlantDeclare)GameObject.FindObjectOfType<SeedPouch>().GetCropToPlace();

        switch (tempCropType)
        {
            case (ObjectiveCrops.PlantDeclare.Beetroot):
            {
                cropChild = Instantiate(types.AllowedPlotObjects[0]);
                cropChild.transform.SetParent(gameObject.transform);
                break;
            }

            case (ObjectiveCrops.PlantDeclare.Pumpkin):
            {
                cropChild = Instantiate(types.AllowedPlotObjects[1]);
                cropChild.transform.SetParent(gameObject.transform);
                break;
            }

            case (ObjectiveCrops.PlantDeclare.Turnip):
            {
                cropChild = Instantiate(types.AllowedPlotObjects[2]);
                cropChild.transform.SetParent(gameObject.transform);
                break;
            }

            case (ObjectiveCrops.PlantDeclare.Corn):
            {
                cropChild = Instantiate(types.AllowedPlotObjects[3]);
                cropChild.transform.SetParent(gameObject.transform);
                break;
            }
            case (ObjectiveCrops.PlantDeclare.Tomato):
                {
                    cropChild = Instantiate(types.AllowedPlotObjects[4]);
                    cropChild.transform.SetParent(gameObject.transform);
                    break;
                }
        }

        if (cropChild != null)
        {


            var obj = cropChild.GetComponent<PlantPlotObject>();

            obj.plantType = tempCropType;
            //obj.OnCreated();
            obj.SetOwningPlot(this);
            GameObject.FindObjectOfType<SeedPouch>().RemoveFirstSeed();
            //currentObject = obj.transform.parent.gameObject.GetComponent<PlotObject>();
        }
    }

    public void OnPlotObjectDestroyed()
    {
        cropChild.gameObject.GetComponent<PlantPlotObject>().OnDestroyed();
        cropChild = null;
    }

    public void PlotAction()
    {
        if (cropChild == null)
        {
            Spawn();
        }
        else
        {
            var obj = cropChild.GetComponent<PlantPlotObject>();
                      
            if (obj != null)
            {
                DestroyPlant();
            }
        }
    }

    private void DestroyPlant()
    {
         var obj = cropChild;

         if (obj != null)
         {
             obj.gameObject.GetComponent<PlantPlotObject>().OnDestroyed();
             //Spawn a new plant after harvesting
             Spawn();
         }
        
    }
    
}