﻿using UnityEngine;
using System.Collections;

public class DirtDespawn : MonoBehaviour {
	
	void Start ()
    {
         Destroy(this.gameObject, Random.Range(0.2f, 1.5f));
	}
}
