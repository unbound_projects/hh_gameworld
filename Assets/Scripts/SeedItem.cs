﻿using UnityEngine;
using System.Collections;

public class SeedItem : MonoBehaviour {

    public ObjectiveCrops.PlantDeclare PlantType;
    public bool occupied = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CallCombine()
    {
        transform.GetComponentInParent<SeedPouch>().CombineSeeds(PlantType);

    }
}
