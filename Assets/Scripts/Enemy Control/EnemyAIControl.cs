﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class EnemyAIControl : MonoBehaviour
{
    enum AI_STATES
    {
        FIND_CROP,
        EAT_CROP,
        MOVE_TO_CROP,
        DEAD
    };

    enum AGENT_TYPE
    {
        PIG = 1,
        COW = 2,
        RABBIT = 3,
        GRUB = 4
    };
    
    AI_STATES thisState;
    public int typeOfAnimal;
    AGENT_TYPE thisType;

    public ObserverObject observer;
    public int maxHealth;

    public int actionCompletionTime;

    public int currentGridId = -1;
    public PlantPlotObject target = null;
    
    public float actionTimer = 0.5f;
    public float actionCounter = 0;
    public LayerMask mask;
    public GameObject shooter;
    public EnemySpawner mySpawner;
    public NavMeshAgent nav;
    private int health;

    private TimerObject timer;

    void Start()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        timer = GameObject.Find("MessageHandler").GetComponent<TimerObject>();

        observer.AddSubscriber("AssignSpawnerID", gameObject, "AssignSpawner");
    

        thisType = (AGENT_TYPE)typeOfAnimal;
        nav = gameObject.GetComponent<NavMeshAgent>();
        health = maxHealth;
        DoAI(AI_STATES.FIND_CROP);
    }

    void Update()
    {
        if(target == null)
        {
            DoAI(AI_STATES.FIND_CROP);
        }
        else if(actionCounter > actionTimer)
        {
            this.transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
            if (target != null && thisState == AI_STATES.FIND_CROP)
            {
                nav.Resume();
                DoAI(AI_STATES.MOVE_TO_CROP);
            }
            else if (target != null && (target.gameObject.transform.position - gameObject.transform.position).magnitude < 1.4f && thisState == AI_STATES.MOVE_TO_CROP)
            {
                nav.Stop();
                DoAI(AI_STATES.EAT_CROP);
            }
            else if (target != null &&  thisState == AI_STATES.EAT_CROP)
            {
                DoAI();
            }
            actionCounter = 0;
        }
        actionCounter += Time.deltaTime;
    }

    public void GetHit()
    {
        health -= 10;
        if(health <= 0)
        {
            this.GetComponent<BoxCollider>().enabled = false;
            timer.RemoveTimerFromObject(gameObject, "EatingTimer");

            //We would make them run away but for now lets just have it so it dies
            Destroy(gameObject);
        }
    }
    void DoFindCrop()
    {
        if (thisType == AGENT_TYPE.PIG)
        {
            RaycastHit[] hits;
            if (mySpawner != null)
            {
                Vector3 tempPos = new Vector3(mySpawner.transform.position.x + (transform.position.x * -mySpawner.transform.forward.x), transform.position.y, mySpawner.transform.position.z + (transform.position.z * mySpawner.transform.forward.z));
                hits = Physics.RaycastAll(transform.position, mySpawner.transform.forward, 50, mask);
                //sorts the hits so its in order instead of unordered.
                if(hits.Length > 0)
                {
                        float min = 999f; //lets assume that the minimum is at the 0th place
                        int minIndex = 0; //store the index of the minimum because thats hoow we can find our object

                        for (int i = 0; i < hits.Length; ++i)// iterate from the 1st element to the last.(Note that we ignore the 0th element)
                        {
                            if (hits[i].distance < min) //if we found smaller distance and its not the player we got a new minimum
                            {
                                if (hits[i].transform.gameObject.GetComponent<FarmPlot>().isCrop)
                                {
                                    if (hits[i].transform.gameObject.GetComponent<FarmPlot>().cropChild != null)
                                    {
                                        min = hits[i].distance; //refresh the minimum distance value
                                        minIndex = i; //refresh the distance
                                    }
                                }
                                else
                                {
                                    min = hits[i].distance; //refresh the minimum distance value
                                    minIndex = i; //refresh the distance
                                }

                            }
                        }

                        if (hits[minIndex].transform.gameObject.GetComponent<FarmPlot>().isCrop)
                        {
                            if (hits[minIndex].transform.gameObject.GetComponent<FarmPlot>().cropChild != null)
                            {
                                target = hits[minIndex].transform.gameObject.GetComponent<FarmPlot>().cropChild.GetComponent<PlantPlotObject>();
                            }
                        }
                        else if (!hits[minIndex].transform.gameObject.GetComponent<FarmPlot>().isCrop)
                         {
                            nav.SetDestination(hits[minIndex].transform.position);
                            nav.Resume();
                        }
                    
                }
               
            }
        }

        if (thisType == AGENT_TYPE.COW)
        {
            observer.DoEvent("FindAnimalTarget", gameObject);
            // Find next plot in front of you
            // If it's not empty, eat plant
            // If it's empty, find next one
        }

        if (thisType == AGENT_TYPE.RABBIT)
        {
            observer.DoEvent("FindAnimalTarget", gameObject);
            //Find a random plot
            // If it's not full, find another random plot
            // If it's full, eat the plant & run off screen
            // Once off screen, begin again
        }

        if (thisType == AGENT_TYPE.GRUB)
        {
            observer.DoEvent("FindAnimalTarget", gameObject);
        }
    }

    void PrepareToEat()
    {
        timer.AddTimer("EatingTimer", gameObject, actionCompletionTime, "DoEatCrop");
    }

    void DoEatCrop()
    {
        if (thisType != AGENT_TYPE.GRUB)
        {
            if (target.health > 0)
            {
                target.health -= 10;
            }
        }
        if (thisType == AGENT_TYPE.GRUB)
        {
            if (target != null)
            {
                // Do Grub Effect thing on target
                ApplyGrubEffect();
                return;
            }
        }


        if(target.health <= 0 || target == null)
        {
            target = null;
            DoAI(AI_STATES.FIND_CROP);
        }
    }

    void ApplyGrubEffect()
    {
        observer.DoEvent("GrubEffectApplied", gameObject);
    }

    void DoMoveToCrop()
    {
        // Set the navmesh agents destination to be the closest found plot
        nav.SetDestination(target.gameObject.transform.position);
    }

    void PrepareDeath()
    {
        // Play the enemy death animation


        // Create timer post animation before destroying enemy model
        float destroyTimer = 1.0f;
        timer.AddTimer("DestroyEnemy", gameObject, destroyTimer, "DoDead");
    }

    void DoDead()
    {

    }

    void DoFindCropStateCheck()
    {
        if (DoGenericStateCheck())
            return;


    }

    void DoEatCropStateCheck()
    {
        if (DoGenericStateCheck())
            return;


    }

    void DoMoveToCropStateCheck()
    {
        if (DoGenericStateCheck())
            return;


    }

    void DoDeadStateCheck()
    {
        if (DoGenericStateCheck())
            return;


    }

    bool DoGenericStateCheck()
    {
        if (health <= 0)
            DoDead();

        return false;
    }

    void DoAI()
    {
        switch (thisState)
        {
            case AI_STATES.FIND_CROP:
                {
                    DoFindCrop();
                    DoFindCropStateCheck();
                    break;
                }

            case AI_STATES.MOVE_TO_CROP:
                {
                    DoMoveToCrop();
                    DoMoveToCropStateCheck();
                    break;
                }

            case AI_STATES.EAT_CROP:
                {
                    PrepareToEat();
                    DoEatCropStateCheck();
                    break;
                }

            case AI_STATES.DEAD:
                {
                    DoDead();
                    DoDeadStateCheck();
                    break;
                }
        }
    }
    void DoAI(AI_STATES state)
    {
        thisState = state;
        switch (thisState)
        {
            case AI_STATES.FIND_CROP:
            {
                DoFindCrop();
                DoFindCropStateCheck();
                break;
            }

            case AI_STATES.MOVE_TO_CROP:
            {
                DoMoveToCrop();
                DoMoveToCropStateCheck();
                break;
            }

            case AI_STATES.EAT_CROP:
            {
                PrepareToEat();
                DoEatCropStateCheck();
                break;
            }

            case AI_STATES.DEAD:
            {
                DoDead();
                DoDeadStateCheck();
                break;
            }
        }
    }

    void AssignSpawner(EnemySpawner spawnerUsed)
    {
        //mySpawner = spawnerUsed;
    }
}