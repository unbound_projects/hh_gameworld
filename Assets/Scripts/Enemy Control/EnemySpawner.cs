﻿using UnityEngine;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour
{
    public List<GameObject> enemyPrefabs = new List<GameObject>();
    public List<GameObject> plotsForPigs = new List<GameObject>();

    [HideInInspector]
    public bool canSpawn = false;

    public int currentGridID;

    private ObserverObject observer;
    private TimerObject timer;
    private float spawnTimer;
	private int spawnSelect;

    public float spawnTimeMin;
    public float spawnTimeMax;

	//public GameObject[] spawns;

    void Start ()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();
        timer = GameObject.Find("MessageHandler").GetComponent<TimerObject>();
        spawnTimer = Random.Range(spawnTimeMin, spawnTimeMax);

        PrepareTimer();
	}

    void PrepareTimer()
    {
        timer.AddTimer("SpawnTimer", gameObject, spawnTimer, "SpawnEnemy", true);
    }

    void SpawnEnemy()
    {
        int randVal = Random.Range(0, enemyPrefabs.Count);
        spawnTimer = Random.Range(spawnTimeMin, spawnTimeMax);
		//spawnSelect = Random.Range(0, spawns.Length);

        if (randVal != -1 && canSpawn)
        {
			GameObject enemySpawned = (GameObject)Instantiate(enemyPrefabs[randVal], /*spawns[spawnSelect].*/transform.position, transform.rotation);
            enemySpawned.GetComponent<EnemyAIControl>().currentGridId = currentGridID;
            enemySpawned.GetComponent<EnemyAIControl>().mySpawner = gameObject.GetComponent<EnemySpawner>();
            canSpawn = false;
            observer.DoEvent("AssignSpawnerID", this);   
        }
    }

    public EnemySpawner GetSpawner()
    {
        return this;
    }
}
