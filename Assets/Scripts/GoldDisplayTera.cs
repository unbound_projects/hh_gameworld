﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoldDisplayTera : MonoBehaviour {

    ObserverObject observer;
	// Use this for initialization
	void Start ()
    {
        observer = GameObject.FindObjectOfType<ObserverObject>();
        observer.AddSubscriber("PurchasedCrop", gameObject, "ShowGold");
        ShowGold();
    }
	
    void ShowGold()
    {
        GetComponent<Text>().text = GameObject.FindObjectOfType<GameSystem>().dataPlayer.currency.ToString();
    }
}
