﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class StarDisplay : MonoBehaviour
{
    public List<Sprite> spriteHolder;

    private ObserverObject observer;

    void Start()
    {
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();

        observer.AddSubscriber("LevelComplete", gameObject, "DisplaySprite");
    }

    public void DisplaySprite(int id)
    {
        if (id > spriteHolder.Count)
            return;

        GetComponent<Image>().sprite = spriteHolder[id];
    }
}
