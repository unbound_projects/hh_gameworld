﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainMenuControl : MonoBehaviour
{
    public List<Canvas> canvasList = new List<Canvas>();
    public List<GameObject> seasonHolders = new List<GameObject>();

    public GameSystem system;
    public GameObject seasonIcon;
    public GameObject seasonSelect;
    public Canvas storeCanvas;
    public Canvas deckCanvas;
    public Canvas mainCanvas;
    private bool storeActive = false;
    private bool deckActive = false;
    void Start()
    {
        system = GameObject.FindObjectOfType<GameSystem>();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadLevelSelect()
    {
        foreach (Canvas thisCanvas in canvasList)
        {
            thisCanvas.gameObject.SetActive(false);
        }

        canvasList[2].gameObject.SetActive(true);
    }

    public void LoadGameWorld(LevelData level)
    {
        system.SetLevel(level);
        SceneManager.LoadScene(1);
        //ActivateCanvas(1);
    }
 
    void ActivateCanvas(int currCanvas)
    {
        if (system.currentLvl != null)
        {
            foreach (Canvas thisCanvas in canvasList)
            {
                thisCanvas.gameObject.SetActive(false);
            }

            canvasList[currCanvas].gameObject.SetActive(true);
        }
    }

    public void SeasonSelect(int i)
    {
        seasonHolders[i].gameObject.SetActive(true);
        seasonIcon.SetActive(false);
    }
    public void BackToSelection()
    {
        seasonSelect.SetActive(true); 
        seasonIcon.SetActive(false);
    }
    public void FaceBookLink()
    {
        Application.OpenURL("https://www.facebook.com/hecticharvest/"); 
    }
    public void StoreCanvas()
    {
        storeCanvas.gameObject.SetActive(!storeCanvas.gameObject.activeSelf);
        storeActive = !storeActive;

        deckCanvas.gameObject.SetActive(false);
        deckActive = false;
    }
    public void DeckCanvas()
    {
        deckActive = !deckActive;
        deckCanvas.gameObject.SetActive(!deckCanvas.gameObject.activeSelf);

        storeCanvas.gameObject.SetActive(false);
        storeActive = false;
    }

    public void MainMenu()
    {
        mainCanvas.gameObject.SetActive(true);
        mainCanvas.transform.GetChild(0).transform.gameObject.SetActive(true);
        mainCanvas.transform.GetChild(1).transform.gameObject.SetActive(false);
        foreach (Canvas thisCanvas in canvasList)
        {
            thisCanvas.gameObject.SetActive(false);
        }
    }
}