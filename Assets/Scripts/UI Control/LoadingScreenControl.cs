﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingScreenControl : MonoBehaviour
{
    public float loadCompletionTime;

    private TimerObject timer;
    

    // Use this for initialization
    void Start ()
    {
        timer = GameObject.Find("MessageHandler").GetComponent<TimerObject>();
        GameObject.Find("Side Bar").SetActive(false);
        PrepareToLoad();
	}
	

    void PrepareToLoad()
    {
        timer.AddTimer("LoadLevel", gameObject, loadCompletionTime, "LoadFirstScene");
    }

    void LoadFirstScene()
    {
        StartCoroutine(LevelCoroutine());
    }


    IEnumerator LevelCoroutine()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(1);

        while (!async.isDone)
        {
            //LoadingBar.fillAmount = async.progress / 0.9f;
            yield return null;
        }
    }
}

