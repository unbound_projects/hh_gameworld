﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SystemCounter : MonoBehaviour {

    private GameSystem system;
    public Text text1;
    public Text text2;
    public Text text3;

    // Use this for initialization
    void Start ()
    {
        system = GameObject.FindObjectOfType<GameSystem>();
       Vector3 data = system.GetStarValue();
        text1.text = data.x.ToString();
        text2.text = data.y.ToString();
        text3.text = data.z.ToString();
    }
}
