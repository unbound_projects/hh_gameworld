﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class DeckDisplay : MonoBehaviour
{
    GameSystem system;
    int startIndex = 0;
    public bool deckMode = true;
    public int deckIndex = 0;
    public List<GameObject> deckCardListDisplay = new List<GameObject>();
    // Use this for initialization
    void OnEnable()
    {
        system = GameObject.FindObjectOfType<GameSystem>();

        for (int i = startIndex; i < startIndex + 8; ++i)
        {
            if (system.dataPlayer.unlockedCrops.Count > i)
            {
                transform.GetChild(i).GetChild(0).GetComponent<Image>().enabled = true;
                transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = system.GetCropSprite(system.dataPlayer.unlockedCrops[i].seedType);
                CropCard tempCard = new CropCard();
                tempCard.cropType = system.dataPlayer.unlockedCrops[i].seedType;
                transform.GetChild(i).GetComponent<CardSlot>().plantType = tempCard;
            }
        }

        DisplayDeck();
    }

    public void CardSelect(CropCard type)
    {
        if (deckMode)
        {
            if (type != null)
            {
                if (system.dataPlayer.deckList[0].cardList.Count >= 4)
                {
                    system.dataPlayer.deckList[0].cardList.RemoveAt(0);
                }
                system.dataPlayer.deckList[0].cardList.Add(type);
                DisplayDeck();
                system.dataPlayer.UpdateXML(system.playerSaveDataPath, system.dataPlayerDefaultsPath);
            }
        }
    }

    public void DisplayDeck()
    {
        if (system.dataPlayer != null)
        {
            for (int i = 0; i < deckCardListDisplay.Count; ++i)
            {
                if (i < system.dataPlayer.deckList[0].cardList.Count)
                {
                    deckCardListDisplay[i].GetComponent<Image>().sprite = system.GetCropSprite(system.dataPlayer.deckList[deckIndex].cardList[i].cropType);
                }
            }
        }
    }
}
