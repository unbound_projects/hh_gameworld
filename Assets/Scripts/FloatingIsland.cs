﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FloatingIsland : MonoBehaviour {

    public GameObject promptWindow;

    private GameSystem system;
    public PreviewLevelCanvas prevCan;

    void OnEnable()
    {
        RefreshVisual();
    }

    public void RefreshVisual()
    {
        system = GameObject.FindObjectOfType<GameSystem>();
        switch (system.currentSeason)
        {
            case LevelData.Seasons.SUMMER:
                for (int i = 0; i < 4; ++i)
                {
                    if (system.summerLevels[i].isUnlocked)
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
                    }
                    else
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                    }
                }
                break;

            case LevelData.Seasons.AUTUMN:
                for (int i = 0; i < 4; ++i)
                {
                    if (system.autumnLevels[i].isUnlocked)
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
                    }
                    else
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                    }
                }
                break;

            case LevelData.Seasons.WINTER:
                for (int i = 0; i < 4; ++i)
                {
                    if (system.winterLevels[i].isUnlocked)
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
                    }
                    else
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                    }
                }
                break;

            case LevelData.Seasons.SPRING:
                for (int i = 0; i < 4; ++i)
                {
                    if (system.springLevels[i].isUnlocked)
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
                    }
                    else
                    {
                        transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                    }
                }
                break;
        }

    }
    public void Selected(int id)
    {
        bool continueCheck = false;
        switch (system.currentSeason)
        {
            case LevelData.Seasons.SUMMER:
                if (id < system.summerLevels.Count)
                {
                    if (system.summerLevels[id].isUnlocked)
                    {
                        prevCan.currentLevel = system.summerLevels[id];
                        continueCheck = true;
                    }
                }
                break;

            case LevelData.Seasons.AUTUMN:
                if (id < system.autumnLevels.Count)
                {
                    if (system.autumnLevels[id].isUnlocked)
                    {
                        prevCan.currentLevel = system.autumnLevels[id];
                        continueCheck = true;
                    }
                }
                break;

            case LevelData.Seasons.WINTER:
                if (id < system.winterLevels.Count)
                {
                    if (system.winterLevels[id].isUnlocked)
                    {
                        prevCan.currentLevel = system.winterLevels[id];
                        continueCheck = true;
                    }
                }
                break;

            case LevelData.Seasons.SPRING:
                if (id < system.springLevels.Count)
                {
                    if (system.springLevels[id].isUnlocked)
                    {
                        prevCan.currentLevel = system.springLevels[id];
                        continueCheck = true;
                    }
                }
                break;
        }
        if (continueCheck)
        {
            promptWindow.GetComponent<PreviewLevelCanvas>().id = id;
            promptWindow.SetActive(true);
        }
    }
}
