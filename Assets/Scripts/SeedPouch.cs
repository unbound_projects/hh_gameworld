﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SeedPouch : MonoBehaviour {

    public List<GameObject> seedList = new List<GameObject>();
    private List<ObjectiveCrops.PlantDeclare> allowedCrops = new List<ObjectiveCrops.PlantDeclare>();
    private List<int> chanceWheel = new List<int>();
	// Use this for initialization
	void Awake ()
    {
        for(int i = 0; i < seedList.Count;++i)
        {
            seedList[i].transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 0);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(allowedCrops.Count == 0)
        {
            allowedCrops = GameObject.FindObjectOfType<PlotTypes>().presentCrops;
            for (int i = 0; i < allowedCrops.Count; ++i)
            {
                chanceWheel.Add(100);
            }
            AddSeed(3);
        }
	}

    public void AddSeed(int amount)
    {
        for (int j = 0; j < amount; ++j)
        {
            int totalAmount = 0;
            for (int i = 0; i < chanceWheel.Count; ++i)
            {
                totalAmount += chanceWheel[i];
            }

            int randVal = Random.Range(0, totalAmount);
            int top = 0;
            for (int i = 0; i < chanceWheel.Count; ++i)
            {
                top += chanceWheel[i];
                if (randVal < top && GetNextClearIndex() != -1)
                {
                    seedList[GetNextClearIndex()].transform.GetChild(0).GetComponent<Image>().sprite = GameObject.FindObjectOfType<GameSystem>().GetCropSprite(allowedCrops[i]);
                    seedList[GetNextClearIndex()].transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 255);
                    seedList[GetNextClearIndex()].GetComponent<SeedItem>().PlantType = allowedCrops[i];
                    seedList[GetNextClearIndex()].GetComponent<SeedItem>().occupied = true;
                    break;
                }
            }
        }
    }
    public int GetNextClearIndex()
    {
        for (int i = 0; i < seedList.Count; ++i)
        {
            if (seedList[i].GetComponent<SeedItem>().occupied == false)
                return i;
        }

        return -1;
    }

    public ObjectiveCrops.PlantDeclare? GetCropToPlace()
    {
        if (seedList[0].GetComponent<SeedItem>().occupied == true)
            return seedList[0].GetComponent<SeedItem>().PlantType;
        else
            return null;
    }
    public void RemoveFirstSeed()
    {
        for (int i = 0; i < seedList.Count; ++i)
        {
            if (i + 1 < seedList.Count)
            {
                seedList[i].transform.GetChild(0).GetComponent<Image>().sprite = seedList[i + 1].transform.GetChild(0).GetComponent<Image>().sprite;
                seedList[i].transform.GetChild(0).GetComponent<Image>().color = seedList[i + 1].transform.GetChild(0).GetComponent<Image>().color;
                seedList[i].GetComponent<SeedItem>().PlantType = seedList[i + 1].GetComponent<SeedItem>().PlantType;
                seedList[i].GetComponent<SeedItem>().occupied = seedList[i + 1].GetComponent<SeedItem>().occupied;
            }
            else
            {
                seedList[i].transform.GetChild(0).GetComponent<Image>().color = new Vector4(255,255,255,0);
                seedList[i].GetComponent<SeedItem>().occupied = false;
            }
        }
        AddSeed(1);
    }

    public void RemoveSeedAt(int index)
    {
        for (int i = index; i < seedList.Count; ++i)
        {
            if (i + 1 < seedList.Count)
            {
                seedList[i].transform.GetChild(0).GetComponent<Image>().sprite = seedList[i + 1].transform.GetChild(0).GetComponent<Image>().sprite;
                seedList[i].transform.GetChild(0).GetComponent<Image>().color = seedList[i + 1].transform.GetChild(0).GetComponent<Image>().color;
                seedList[i].GetComponent<SeedItem>().PlantType = seedList[i + 1].GetComponent<SeedItem>().PlantType;
                seedList[i].GetComponent<SeedItem>().occupied = seedList[i + 1].GetComponent<SeedItem>().occupied;
            }
            else
            {
                seedList[i].transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 0);
                seedList[i].GetComponent<SeedItem>().occupied = false;
            }
        }
    }
    public void CombineSeeds(ObjectiveCrops.PlantDeclare plantType)
    {
        List<GameObject> listToReduce = new List<GameObject>();
        for(int i = 0; i < seedList.Count; ++i)
        {
            if(seedList[i].GetComponent<SeedItem>().PlantType == plantType)
            {
                listToReduce.Add(seedList[i]);
            }
        }

        if(listToReduce.Count > 2)
        {
          
            for(int i = 0; i < listToReduce.Count; ++i)
            {
                int index = listToReduce.Count - i - 1;
                if (index != 0)
                {
                    RemoveSeedAt(seedList.IndexOf(listToReduce[index]));
                }
            }

        }
        

        AddSeed(1);
    }

    public void AdjustRate()
    {

    }

}
