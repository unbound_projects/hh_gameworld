﻿using UnityEngine;
using System.Collections;

public class PopUpTera : MonoBehaviour {

    public GameObject Tera;
    public GameObject warningSign;
    public GameObject shopCrop;
    public ObserverObject observer;
    private TerrariumPlantObject theCrop;

    public void Start()
    {
        observer = GameObject.FindObjectOfType<ObserverObject>();
    }
    public void LockedCropData(int id)
    {
        theCrop = GameObject.FindObjectOfType<GameSystem>().lCrops[id];

    }
    public void UnlockedCropData(int id)
    {
        theCrop = GameObject.FindObjectOfType<GameSystem>().ulCrops[id];
    }


    public void PurchaseCrop()
    {
        GameSystem system = GameObject.FindObjectOfType<GameSystem>();
        //SUBTRACT GOLD AWAY FROM THING
        if (system.dataPlayer.currency >= theCrop.seedCost)
        {
            system.dataPlayer.currency -= theCrop.seedCost;
            system.ulCrops.Add(theCrop);
            system.lCrops.Remove(theCrop);
            observer.DoEvent("PurchasedCrop", null);

            system.dataPlayer.UpdateXML(system.playerSaveDataPath, system.dataPlayerDefaultsPath);
            CloseCanvas();
        }
        else
        {
            warningSign.SetActive(true);
            Invoke("SetSignFalse", 2.0f);
            //Add event to show that you dont actually have a thing

        }
    }

    public void CloseCanvas()
    {
        GameSystem system = GameObject.FindObjectOfType<GameSystem>();
        //IF TRHERE IS STUFF TO RESET TO HERE PLS
        system.dataPlayer.UpdateXML(system.playerSaveDataPath, system.dataPlayerDefaultsPath);
        gameObject.SetActive(false);
    }


    //Just to make invoke work for now
    public void SetSignFalse()
    {
        warningSign.SetActive(false);
    }
}
