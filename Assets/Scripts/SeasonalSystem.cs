﻿using UnityEngine;
using System.Collections.Generic;

public class SeasonalSystem : MonoBehaviour
{
    private TimerObject timer;
    private ObserverObject observer;
    public float damageTimeInterval;
    public float seasonalDamage;

    private PlantPlotObject[] plants;
    private List<PlantPlotObject> plantsList;
    LevelData.Seasons currentSeason;

    enum SeasonalEffects
    {
        TICK_DMG = 1,
        PLANT_CD = 2,

    }

    SeasonalEffects currentEffect;
        
    void Start()
    {
        timer = GameObject.Find("MessageHandler").GetComponent<TimerObject>();
        observer = GameObject.Find("MessageHandler").GetComponent<ObserverObject>();

        observer.AddSubscriber("AssignSeason", gameObject, "UpdateSeason");

        plants = GameObject.FindObjectsOfType<PlantPlotObject>();
        plantsList = new List<PlantPlotObject>(plants);
    }

    void Update()
    {
        for (int i = 0; i < plants.Length; i++)
        {
            plants[i] = null;
        }

        plants = GameObject.FindObjectsOfType<PlantPlotObject>();

        if (plants.Length != plantsList.Count)
        {
            plantsList.Clear();
            for (int i = 0; i < plants.Length; i++)
            {
                plantsList.Add(plants[i]);
            }
        }

        for (int i = 0; i < plantsList.Count; i++)
        {
            // NEGATIVE SEASONAL EFFECT
            if (!plantsList[i].hasSeasonalTimer && plantsList[i].unfavouredSeason == currentSeason)
            {
                switch (plantsList[i].plantType)
                {
                    case ObjectiveCrops.PlantDeclare.Beetroot:
                    {
                        currentEffect = SeasonalEffects.TICK_DMG;
                        break;
                    }

                    case ObjectiveCrops.PlantDeclare.Corn:
                    {
                        currentEffect = SeasonalEffects.TICK_DMG;
                        break;
                    }

                    case ObjectiveCrops.PlantDeclare.Pumpkin:
                    {
                        currentEffect = SeasonalEffects.TICK_DMG;
                        break;
                    }

                    case ObjectiveCrops.PlantDeclare.Turnip:
                    {
                        currentEffect = SeasonalEffects.TICK_DMG;
                        break;
                    }
                    case ObjectiveCrops.PlantDeclare.Tomato:
                        {
                            currentEffect = SeasonalEffects.TICK_DMG;
                            break;
                        }
                }
                //observer.DoEvent("ApplyNegativeSeasonalEffect", currentEffect);
            }            

            // POSITIVE SEASONAL EFFECT
            if (!plantsList[i].hasSeasonalTimer && plantsList[i].favouredSeason == currentSeason)
            {
                switch (plantsList[i].plantType)
                {
                    case ObjectiveCrops.PlantDeclare.Beetroot:
                        {
                            break;
                        }

                    case ObjectiveCrops.PlantDeclare.Corn:
                        {
                            break;
                        }

                    case ObjectiveCrops.PlantDeclare.Pumpkin:
                        {
                            break;
                        }

                    case ObjectiveCrops.PlantDeclare.Turnip:
                        {
                            break;
                        }
                    case ObjectiveCrops.PlantDeclare.Tomato:
                        {
                            break;
                        }
                }
                //observer.DoEvent("ApplyPositiveSeasonalEffect", currentEffect);
            }

        }
    }

    public float GetDamage()
    {
        return seasonalDamage;
    }

    private void UpdateSeason(LevelData.Seasons seasonPassed)
    {
        currentSeason = seasonPassed;
    }
}