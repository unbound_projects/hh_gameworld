﻿using UnityEngine;
using System.Collections;

public class CardSlot : MonoBehaviour {
    public CropCard plantType;

    public void OnSelect()
    {
        GetComponentInParent<DeckDisplay>().CardSelect(plantType);
    }
}
