﻿using UnityEngine;
using System.Collections;

public class EnemyDelete : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void OnTriggerEnter(Collider other)
    {

       if( other.gameObject.CompareTag("Animal"))
        {
            Destroy(other.gameObject);
        }
    }
}
