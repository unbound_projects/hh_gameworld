﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;


public class TerrariumPlantObject
{
    //[XmlAttribute("PlantType")]
    [XmlAttribute("Cost")]
    public int seedCost;
    [XmlAttribute("Type")]
    public ObjectiveCrops.PlantDeclare seedType;
    [XmlAttribute("Level")]
    public int seedLevel;
    [XmlAttribute("RW")]
    public int resistanceWinter;
    [XmlAttribute("RS")]
    public int resistanceSummer;
    [XmlAttribute("RA")]
    public int resistanceAutmn;
    [XmlAttribute("RSp")]
    public int resistanceSpring;
    [XmlAttribute("FS")]
    public LevelData.Seasons favoredSeason;
    [XmlAttribute("UFS")]
    public LevelData.Seasons unFavoredSeason;
    // Probably needs more stuff lmao
}