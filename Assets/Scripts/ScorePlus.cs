﻿using UnityEngine;
using System.Collections;

public class ScorePlus : MonoBehaviour {
    public float desResponseTimer = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        desResponseTimer += Time.deltaTime;
        transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime * 2, transform.position.z);
        GetComponent<TextMesh>().color = new Vector4(255,255,255, GetComponent<TextMesh>().color.a - 0.02f);

        if (desResponseTimer > 3f)
        {
            Destroy(gameObject);
        }
    }
}
