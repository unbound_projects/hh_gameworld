﻿Shader "Custom/CutOff" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_FillAmount("FillAmount", Range(0,1)) = 1.0
		_FlashColor("Flash Color", Color ) = (1,0,0,1)
		_FlashSpeed("Flash Speed", Range(1, 5) ) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Transparent" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		//#pragma surface surf Standard fullforwardshadows
		#pragma surface surf Lambert alpha
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;
		fixed4 _FlashColor;
		float _FillAmount;


		void surf (Input IN, inout SurfaceOutput o) {

			float pi = 3.1415;
			fixed2 uv = IN.uv_MainTex - fixed2(0.5, 0.5);
			fixed2 forward = fixed2(0, 1);

			float angle = acos(dot(uv, forward) / (length(uv) * length(forward)));
			//o.Albedo = fixed3(angle, angle, angle);
			//o.Alpha = 1.0f;
			float sTime = (sin(_Time.a * 2) + 1) * 0.5f;
			o.Albedo = fixed3(sTime, sTime, sTime);
			o.Alpha = 1.0f;

			if (angle > pi * _FillAmount)
			{
				o.Albedo = fixed3(0,0,0);
				// Metallic and smoothness come from slider variables
				o.Alpha = 0;
			}
			else
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				// Metallic and smoothness come from slider variables
				//o.Alpha = c.a;
				o.Alpha = (_FillAmount * _FillAmount) * tex2D(_MainTex, IN.uv_MainTex).a;
			}
			//Albedo comes from a texture tinted by color

		}
		ENDCG
	}
	FallBack "Diffuse"
}
