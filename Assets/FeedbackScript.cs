﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

public class FeedbackScript : MonoBehaviour
{
    [HideInInspector]
    public GameObject parentObject;
    
    bool updateFeedbackPosition = true;
    bool beginMoving = false;
    GameObject objectiveTransform;

    public float FadeBeginTime;
    public float FadeEndTime;

    // Update is called once per frame
    void Start()
    {
         objectiveTransform = GameObject.Find("FakeUI");
         updateFeedbackPosition = true;
    }

	void Update ()
    {
        if (updateFeedbackPosition)
        {
            //transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + Time.deltaTime);
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Clamp(transform.position.z + Time.deltaTime, -7f, transform.position.z / 2.25f));

            StartCoroutine(FadeTo(FadeBeginTime, FadeEndTime));

            //Mathf.Clamp(transform.position.z, -6.5f, -7f);

            if (transform.position.z == -5.75f)
            {
                Debug.Log("Clamp Position");
                updateFeedbackPosition = false;
            }
        }
    }

    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = transform.GetComponent<Renderer>().material.color.a;

        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            transform.GetComponent<Renderer>().material.color = newColor;

            if (transform.GetComponent<Renderer>().material.color.a <= 0.1)
                Destroy(this.gameObject);

            yield return null;
        }
    }
}